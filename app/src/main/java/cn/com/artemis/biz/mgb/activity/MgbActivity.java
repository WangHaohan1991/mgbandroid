package cn.com.artemis.biz.mgb.activity;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.cn.artemis.artemisbase.base.BaseActivity;
import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.cn.artemis.basekit.utils.SharedPreferencesUtil;
import com.cn.artemis.basekit.utils.SwitchLanguageUtils;
import com.cn.artemis.basekit.utils.SystemSetUtils;
import com.cn.atemis.artemisbwv.BaseWebviewHandler;
import com.cn.atemis.artemisbwv.LoadingWebViewclient;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.tencent.sonic.sdk.SonicCacheInterceptor;
import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicConstants;
import com.tencent.sonic.sdk.SonicEngine;
import com.tencent.sonic.sdk.SonicSession;
import com.tencent.sonic.sdk.SonicSessionConfig;
import com.tencent.sonic.sdk.SonicSessionConnection;
import com.tencent.sonic.sdk.SonicSessionConnectionInterceptor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.artemis.biz.mgb.R;
import cn.com.artemis.biz.mgb.SonicRuntimeImpl;
import cn.com.artemis.biz.mgb.SonicSessionClientImpl;
import cn.com.artemis.biz.mgb.common.AndroidWorkaround;
import cn.com.artemis.biz.mgb.common.MgbWebChromeClient;
import cn.com.artemis.biz.mgb.common.MgbWebViewHandler;
import cn.com.artemis.biz.mgb.jshandler.bean.CallJsBean;
import cn.com.artemis.biz.mgb.jshandler.bean.UserInfoBean;
import cn.com.artemis.biz.mgb.view.MgbLoadingErrorView;
import cn.com.artemis.biz.mgb.view.MgbLoadingView;

import static cn.com.artemis.biz.mgb.jshandler.InitRevant.NATIVE_CALL_JS;


public class MgbActivity extends BaseActivity {
    public final static String PARAM_URL = "param_url";

    public final static String PARAM_MODE = "param_mode";

    private SonicSession sonicSession;

    private BridgeWebView mBridgeWebView;
    private BaseWebviewHandler mWebviewHandler;
    private LoadingWebViewclient mWebViewclient;
    private MgbWebChromeClient mKWebChromeClient;
    private FrameLayout mLayout;

    @Override
    protected int getContentLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        hidetoolBar();
        mLayout = findViewById(R.id.web_layout);
        initWebView();
        AndroidWorkaround.assistActivity(mLayout);
    }

    private void initWebView() {
        getWindow().setFlags(//强制打开GPU渲染
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        mBridgeWebView = new BridgeWebView(this);
        mWebviewHandler = new MgbWebViewHandler(this,mBridgeWebView);
        mLayout.addView(mBridgeWebView, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        Intent intent = getIntent();
        String url = intent.getStringExtra(PARAM_URL);
        int mode = intent.getIntExtra(PARAM_MODE, -1);
        if (TextUtils.isEmpty(url) || -1 == mode) {
            finish();
            return;
        }
        // init sonic engine if necessary, or maybe u can do this when application created
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(getApplication()), new SonicConfig.Builder().build());
        }

        SonicSessionClientImpl sonicSessionClient = null;

        // if it's sonic mode , startup sonic session at first time
        if (SplashActivity.MODE_DEFAULT != mode) { // sonic mode
            SonicSessionConfig.Builder sessionConfigBuilder = new SonicSessionConfig.Builder();
            sessionConfigBuilder.setSupportLocalServer(true);

            // if it's offline pkg mode, we need to intercept the session connection
            if (SplashActivity.MODE_SONIC_WITH_OFFLINE_CACHE == mode) {
                sessionConfigBuilder.setCacheInterceptor(new SonicCacheInterceptor(null) {
                    @Override
                    public String getCacheData(SonicSession session) {
                        return null; // offline pkg does not need cache
                    }
                });

                sessionConfigBuilder.setConnectionInterceptor(new SonicSessionConnectionInterceptor() {
                    @Override
                    public SonicSessionConnection getConnection(SonicSession session, Intent intent) {
                        return new OfflinePkgSessionConnection(mContext, session, intent);
                    }
                });
            }

            // create sonic session and run sonic flow
            sonicSession = SonicEngine.getInstance().createSession(url, sessionConfigBuilder.build());
            if (null != sonicSession) {
                sonicSession.bindClient(sonicSessionClient = new SonicSessionClientImpl());
            } else {
                // this only happen when a same sonic session is already running,
                // u can comment following codes to feedback as a default mode.
                // throw new UnknownError("create session fail!");
//                Toast.makeText(this, "create sonic session fail!", Toast.LENGTH_LONG).show();
            }
        }
        // init webview
        if (mWebViewclient == null){
            mWebViewclient = new LoadingWebViewclient(this,mBridgeWebView){
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if (sonicSession != null) {
                        sonicSession.getSessionClient().pageFinish(url);
                    }
                }

                @TargetApi(21)
                @Override
                public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                    return shouldInterceptRequest(view, request.getUrl().toString());
                }

                @Override
                public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                    if (sonicSession != null) {
                        return (WebResourceResponse) sonicSession.getSessionClient().requestResource(url);
                    }
                    return null;
                }

                @Override
                public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                    sslErrorHandler.proceed();
                }
            };
            MgbLoadingErrorView MgbLoadingErrorView = new MgbLoadingErrorView(this);
            mWebViewclient.loadingSetting(new MgbLoadingView(this),MgbLoadingErrorView);
            mBridgeWebView.removeView(MgbLoadingErrorView);
        }
        if(mKWebChromeClient == null){
            mKWebChromeClient = new MgbWebChromeClient(this);
            mKWebChromeClient.setOnLoadingCompeleteCallback(new MgbWebChromeClient.OnLoadingCompeleteCallback() {
                @Override
                public void loadSuccess() {
                    initSystemData();
                }
            });
        }
        mBridgeWebView.setWebViewClient(mWebViewclient);
        mBridgeWebView.setWebChromeClient(mKWebChromeClient);
        initWebViewSettings();

        // webview is ready now, just tell session client to bind
        if (sonicSessionClient != null) {
            sonicSessionClient.bindWebView(mBridgeWebView);
            sonicSessionClient.clientReady();
        } else { // default mode
            mBridgeWebView.loadUrl(url);
        }
    }

    /**
     * 页面加载完成后初始化一些数据
     */
    private void initSystemData() {
        initH5();
        setPermissionDialog();
    }

    private void initH5() {
        CallJsBean callJsBean = new CallJsBean();
        callJsBean.setLanguage(SwitchLanguageUtils.getLanguage(this));
        callJsBean.setUserInfo(JsonHelper.parseObject(SharedPreferencesUtil.getUserInfo(this), UserInfoBean.class));
        CallH5Event event = new CallH5Event();
        event.methodName= NATIVE_CALL_JS;
        event.param= JsonHelper.toJSONString(callJsBean);
        BusHelper.postEvent(event);
    }

    private void setPermissionDialog() {
        //通知权限弹窗
        if (!SystemSetUtils.isNotificationEnabled(this)) {
            showConfirmDialog("权限提示", "是否开启系统通知权限？", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    SystemSetUtils.goToSet(mContext);
                }
            }, null);
        }
    }

    /**
     *  init webview settings
     */
    private void initWebViewSettings() {
        WebSettings webSettings = mBridgeWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        /**
         * 启用mixed content    android 5.0以上默认不支持Mixed Content
         *
         * 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
         * */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if(mKWebChromeClient.callback !=null){
                mKWebChromeClient.hideCustomView();
            } else if (mBridgeWebView.canGoBack()) {
                mBridgeWebView.goBack(); //goBack()表示返回WebView的上一页面
            }else {
                backToScreen();
            }
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }

    private void backToScreen() {
        //返回屏幕
        Intent intent= new Intent(Intent.ACTION_MAIN);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //如果是服务里调用，必须加入new task标识
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBridgeWebView.onPause();
        mBridgeWebView.pauseTimers(); //小心这个！！！暂停整个 WebView 所有布局、解析、JS。
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBridgeWebView.onResume();
        mBridgeWebView.resumeTimers();
    }
    @Override
    protected void onDestroy() {
        if (null != sonicSession) {
            sonicSession.destroy();
            sonicSession = null;
        }
        BusHelper.unRegiste(this);
        mWebviewHandler.release();
        if (mBridgeWebView != null) {
            mBridgeWebView.clearHistory();
            ((ViewGroup) mBridgeWebView.getParent()).removeView(mBridgeWebView);
            mBridgeWebView.loadUrl("about:blank");
            mBridgeWebView.stopLoading();
            mBridgeWebView.setWebChromeClient(null);
            mBridgeWebView.setWebViewClient(null);
            mBridgeWebView.destroy();
            mBridgeWebView = null;
        }
        super.onDestroy();
    }



    private static class OfflinePkgSessionConnection extends SonicSessionConnection {

        private final WeakReference<Context> context;

        public OfflinePkgSessionConnection(Context context, SonicSession session, Intent intent) {
            super(session, intent);
            this.context = new WeakReference<Context>(context);
        }

        @Override
        protected int internalConnect() {
            Context ctx = context.get();
            if (null != ctx) {
                try {
                    InputStream offlineHtmlInputStream = ctx.getAssets().open("sonic-demo-index.html");
                    responseStream = new BufferedInputStream(offlineHtmlInputStream);
                    return SonicConstants.ERROR_CODE_SUCCESS;
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
            return SonicConstants.ERROR_CODE_UNKNOWN;
        }

        @Override
        protected BufferedInputStream internalGetResponseStream() {
            return responseStream;
        }

        @Override
        public void disconnect() {
            if (null != responseStream) {
                try {
                    responseStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getResponseCode() {
            return 200;
        }

        @Override
        public Map<String, List<String>> getResponseHeaderFields() {
            return new HashMap<>(0);
        }

        @Override
        public String getResponseHeaderField(String key) {
            return "";
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mKWebChromeClient.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mKWebChromeClient.onRequestPermissionsResult(requestCode,permissions,grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void  showConfirmDialog(String title, String  message,
                                    DialogInterface.OnClickListener okEvent, DialogInterface.OnClickListener cancelEvent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.comfirm,
                okEvent);
        builder.setNegativeButton(R.string.cancel,
                cancelEvent);
        builder.show();
    }
}
