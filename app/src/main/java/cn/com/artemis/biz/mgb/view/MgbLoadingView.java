package cn.com.artemis.biz.mgb.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;

import com.cn.atemis.artemisbwv.statusview.loadstatus.ILoadingView;

import cn.com.artemis.biz.mgb.R;


/**
 * @dec webview 加载视图
 */
public class MgbLoadingView extends LinearLayout implements ILoadingView {

    public MgbLoadingView(Context context) {
        super(context);
        init(context);
    }


    public MgbLoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        View.inflate(context, R.layout.mgb_widget_loading_view, this);
    }

    @Override
    public void show() {
        this.setVisibility(View.VISIBLE);
    }

    @Override
    public void hide() {
        this.setVisibility(View.GONE);
    }

    @Override
    public View getView() {
        return this;
    }
}
