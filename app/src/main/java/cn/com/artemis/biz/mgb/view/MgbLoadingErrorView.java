package cn.com.artemis.biz.mgb.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;

import com.cn.atemis.artemisbwv.statusview.loadstatus.ILoadingErrorView;

import cn.com.artemis.biz.mgb.R;


public class MgbLoadingErrorView extends LinearLayout implements ILoadingErrorView {

    public MgbLoadingErrorView(Context context) {
        super(context);
        init(context);
    }

    public void init(Context context) {
        View.inflate(context, R.layout.mgb_widget_error_view, this);
    }

    public MgbLoadingErrorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @Override
    public void show() {
        this.setVisibility(View.VISIBLE);
    }

    @Override
    public void hide() {
        this.setVisibility(View.GONE);
    }

    @Override
    public View getView() {
        return this;
    }
}
