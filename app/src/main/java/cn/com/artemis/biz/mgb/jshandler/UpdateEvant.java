package cn.com.artemis.biz.mgb.jshandler;

import android.app.Activity;
import android.content.Context;

import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.cretin.www.cretinautoupdatelibrary.interfaces.AppDownloadListener;
import com.cretin.www.cretinautoupdatelibrary.model.DownloadInfo;
import com.cretin.www.cretinautoupdatelibrary.utils.AppUpdateUtils;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import cn.com.artemis.biz.mgb.bean.VersionBean;

public class UpdateEvant {
    public static final String JS_CALL_FORCE_UPDATE = "forceUpdate";
    public static final String JS_CALL_UPDATE = "update";
    private Context mContext;

    public UpdateEvant(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 版本更新
     * @param bridgeWebView
     */
    public  void initUpdate(BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_FORCE_UPDATE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用强制更新");
                VersionBean versionBean = JsonHelper.parseObject(data, VersionBean.class);
                toUpdate(versionBean, true);
            }
        });
        bridgeWebView.registerHandler(JS_CALL_UPDATE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用提示更新更新");  VersionBean versionBean = JsonHelper.parseObject(data, VersionBean.class);
                toUpdate(versionBean, false);
            }
        });
    }

    private void toUpdate(final VersionBean versionBean, final boolean isForceUpdate) {
        ((Activity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DownloadInfo info = new DownloadInfo().setApkUrl(versionBean.getDownloadPublicUrl())
                        .setFileSize(31338250)
                        .setProdVersionCode(30)
                        .setProdVersionName(versionBean.getVersion())
                        .setMd5Check("68919BF998C29DA3F5BD2C0346281AC0")
                        .setForceUpdateFlag(isForceUpdate ? 1 : 0)
                        .setUpdateLog(versionBean.getContext());
                AppUpdateUtils.getInstance()
                        .checkUpdate(info);
            }
        });
    }
}
