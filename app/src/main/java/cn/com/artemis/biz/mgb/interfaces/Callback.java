package cn.com.artemis.biz.mgb.interfaces;

public interface Callback<T> {
    void onSuccess(T t);
    void onFailed(String msg);
}
