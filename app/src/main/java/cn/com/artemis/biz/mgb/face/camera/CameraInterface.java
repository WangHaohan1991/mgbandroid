package cn.com.artemis.biz.mgb.face.camera;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;


import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.artemisbase.utils.other.LogUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import cn.com.artemis.biz.mgb.face.util.BitmapUtils;
import cn.com.artemis.biz.mgb.face.util.CamParaUtil;
import cn.com.artemis.biz.mgb.face.util.FileUtil;
import cn.com.artemis.biz.mgb.face.util.ImageUtil;
import okio.Okio;

public class CameraInterface {
	private static final String TAG = "whh";
	private Camera mCamera;
	private Camera.Parameters mParams;
	private boolean isPreviewing = false;
	private float mPreviwRate = -1f;
	private int mCameraId = -1;
	private boolean isGoolgeFaceDetectOn = false;
	private static CameraInterface mCameraInterface;

	// 是否是Android 10以上手机
	private boolean isAndroidQ = Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q;

	private Context mContext;

	private CameraCallBack mCameraCallBack;

	public CameraCallBack getmCameraCallBack() {
		return mCameraCallBack;
	}

	public interface CamOpenOverCallback{
		public void cameraHasOpened();
	}


	private CameraInterface(Context context){
		mContext = context;
	}
	public static synchronized CameraInterface getInstance(Context context){
		if(mCameraInterface == null){
			mCameraInterface = new CameraInterface(context);
		}
		return mCameraInterface;
	}
	/**打开Camera
	 * @param callback
	 */
	public void doOpenCamera(CamOpenOverCallback callback, int cameraId){
		Log.i(TAG, "Camera open....");
		mCamera = Camera.open(cameraId);
		mCameraId = cameraId;
		if(callback != null){
			callback.cameraHasOpened();
		}
	}
	/**开启预览
	 * @param holder
	 * @param previewRate
	 */
	public void doStartPreview(SurfaceHolder holder, float previewRate){
		Log.i(TAG, "doStartPreview...");
		if(isPreviewing){
			mCamera.stopPreview();
			return;
		}
		if(mCamera != null){

			mParams = mCamera.getParameters();
			mParams.setPictureFormat(PixelFormat.JPEG);//设置拍照后存储的图片格式
			CamParaUtil.getInstance().printSupportPictureSize(mParams);
			CamParaUtil.getInstance().printSupportPreviewSize(mParams);
			//设置PreviewSize和PictureSize
//			Size pictureSize = CamParaUtil.getInstance().getPropPictureSize(
//					mParams.getSupportedPictureSizes(),previewRate, 800);
			Size pictureSize = findBestPictureSize(mParams);
			mParams.setPictureSize(pictureSize.width, pictureSize.height);
//			Size previewSize = CamParaUtil.getInstance().getPropPreviewSize(
//					mParams.getSupportedPreviewSizes(), previewRate, 800);
			Size previewSize = findBestPreviewSize(mParams);
			mParams.setPreviewSize(previewSize.width, previewSize.height);

			mCamera.setDisplayOrientation(90);

			CamParaUtil.getInstance().printSupportFocusMode(mParams);
			List<String> focusModes = mParams.getSupportedFocusModes();
			if(focusModes.contains("continuous-video")){
				mParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
			}
			mCamera.setParameters(mParams);

			try {
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();//开启预览
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			isPreviewing = true;
			mPreviwRate = previewRate;

			mParams = mCamera.getParameters(); //重新get一次
			Log.i(TAG, "最终设置:PreviewSize--With = " + mParams.getPreviewSize().width
					+ "Height = " + mParams.getPreviewSize().height);
			Log.i(TAG, "最终设置:PictureSize--With = " + mParams.getPictureSize().width
					+ "Height = " + mParams.getPictureSize().height);
		}
	}
	/**
	 * 停止预览，释放Camera
	 */
	public void doStopCamera(){
		if(null != mCamera)
		{
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			isPreviewing = false;
			mPreviwRate = -1f;
			mCamera.release();
			mCamera = null;
		}
	}
	/**
	 * 拍照
	 */
	public void doTakePicture(CameraCallBack cameraCallBack){
		mCameraCallBack = cameraCallBack;
		if(isPreviewing && (mCamera != null)){
			mCamera.takePicture(mShutterCallback, null, mJpegPictureCallback);
//			mCamera.setPreviewCallback(mPreviwCallback);
		}
	}

	/**获取Camera.Parameters
	 * @return
	 */
	public Camera.Parameters getCameraParams(){
		if(mCamera != null){
			mParams = mCamera.getParameters();
			return mParams;
		}
		return null;
	}
	/**获取Camera实例
	 * @return
	 */
	public Camera getCameraDevice(){
		return mCamera;
	}


	public int getCameraId(){
		return mCameraId;
	}







	/*为了实现拍照的快门声音及拍照保存照片需要下面三个回调变量*/
	ShutterCallback mShutterCallback = new ShutterCallback()
			//快门按下的回调，在这里我们可以设置类似播放“咔嚓”声之类的操作。默认的就是咔嚓。
	{
		public void onShutter() {
			// TODO Auto-generated method stub
			Log.i(TAG, "myShutterCallback:onShutter...");
		}
	};
	PictureCallback mRawCallback = new PictureCallback()
			// 拍摄的未压缩原数据的回调,可以为null
	{

		public void onPictureTaken(byte[] data, Camera camera) {
			// TODO Auto-generated method stub
			Log.i(TAG, "myRawCallback:onPictureTaken...");

		}
	};
	PictureCallback mJpegPictureCallback = new PictureCallback()
			//对jpeg图像数据的回调,最重要的一个回调
	{
		public void onPictureTaken(final byte[] data, Camera camera) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						if (isAndroidQ) {
							final Uri photoUri = FileUtil.createImageUri(mContext);
							if (photoUri != null && data != null) {
								Bitmap rawBitmap = null;
								if (data != null) {
									rawBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
									mCamera.stopPreview();
									isPreviewing = false;
								}
								if (rawBitmap != null) {
									Bitmap resultBitmap = mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT ?
											BitmapUtils.rotate(rawBitmap, 270f):  //前置摄像头旋转270°
											BitmapUtils.rotate(rawBitmap, 90f);   //后置摄像头旋转90°
									ContentResolver cr = mContext.getContentResolver();
									ParcelFileDescriptor fd = cr.openFileDescriptor(photoUri, "w");
									FileOutputStream ostream = new FileOutputStream(fd.getFileDescriptor());//写
									Okio.buffer(Okio.sink(ostream)).write(BitmapUtils.toByteArray(resultBitmap)).close();
									((Activity)mContext).runOnUiThread(new Runnable() {
										@Override
										public void run() {
											if (getmCameraCallBack() != null) {
												getmCameraCallBack().callBack(FileUtil.getPath(mContext,photoUri));
											}
										}
									});
								}
							}
						} else {
							final File picFile = FileUtil.createImageFile(false);
							if (picFile != null && data != null) {
								Bitmap rawBitmap = null;
								if (data != null) {
									rawBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
									mCamera.stopPreview();
									isPreviewing = false;
								}
								if (rawBitmap != null) {
									Bitmap resultBitmap = mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT ?
											BitmapUtils.rotate(rawBitmap, 270f):  //前置摄像头旋转270°
											BitmapUtils.rotate(rawBitmap, 90f);   //后置摄像头旋转90°
									Okio.buffer(Okio.sink(picFile)).write(BitmapUtils.toByteArray(resultBitmap)).close();
									((Activity)mContext).runOnUiThread(new Runnable() {
										@Override
										public void run() {
											if (getmCameraCallBack() != null) {
												getmCameraCallBack().callBack(picFile.getAbsolutePath());
											}
										}
									});
								}
							}
						}
						//再次进入预览
						mCamera.startPreview();
						isPreviewing = true;
					}catch (Exception e){
						e.printStackTrace();
						((Activity)mContext).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								ToastSafeUtils.showLongToast(mContext,"获取人脸头像失败！");
								if (getmCameraCallBack() != null) {
									getmCameraCallBack().callBack(null);
								}
								//再次进入预览
								mCamera.startPreview();
								isPreviewing = true;
							}
						});
					}
				}
			}).start();
		}
	};



	private Size findBestPictureSize(Camera.Parameters parameters) {
		int  diff = Integer.MIN_VALUE;
		String pictureSizeValueString = parameters.get("picture-size-values");

		// saw this on Xperia
		if (pictureSizeValueString == null) {
			pictureSizeValueString = parameters.get("picture-size-value");
		}

		if(pictureSizeValueString == null) {
			return  mCamera.new Size(getScreenWH().widthPixels,getScreenWH().heightPixels);
		}

		Log.d("tag", "pictureSizeValueString : " + pictureSizeValueString);
		int bestX = 0;
		int bestY = 0;


		for(String pictureSizeString :pictureSizeValueString.split(","))
		{
			pictureSizeString = pictureSizeString.trim();

			int dimPosition = pictureSizeString.indexOf('x');
			if(dimPosition == -1){
				Log.e(TAG, "Bad pictureSizeString:"+pictureSizeString);
				continue;
			}

			int newX = 0;
			int newY = 0;

			try{
				newX = Integer.parseInt(pictureSizeString.substring(0, dimPosition));
				newY = Integer.parseInt(pictureSizeString.substring(dimPosition+1));
			}catch(NumberFormatException e){
				Log.e(TAG, "Bad pictureSizeString:"+pictureSizeString);
				continue;
			}

			Point screenResolution = new Point(getScreenWH().widthPixels,getScreenWH().heightPixels);

			int newDiff = Math.abs(newX - screenResolution.x)+ Math.abs(newY- screenResolution.y);
			if(newDiff == diff)
			{
				bestX = newX;
				bestY = newY;
				break;
			} else if(newDiff > diff){
				if((3 * newX) == (4 * newY)) {
					bestX = newX;
					bestY = newY;
					diff = newDiff;
				}
			}
		}

		if (bestX > 0 && bestY > 0) {
			return mCamera.new Size(bestX, bestY);
		}
		return null;
	}

	private Size findBestPreviewSize(Camera.Parameters parameters) {

		String previewSizeValueString = null;
		int diff = Integer.MAX_VALUE;
		previewSizeValueString = parameters.get("preview-size-values");

		if (previewSizeValueString == null) {
			previewSizeValueString = parameters.get("preview-size-value");
		}

		if(previewSizeValueString == null) {  // 有些手机例如m9获取不到支持的预览大小   就直接返回屏幕大小
			return  mCamera.new Size(getScreenWH().widthPixels,getScreenWH().heightPixels);
		}
		Log.d("tag", "previewSizeValueString : " + previewSizeValueString);
		int bestX = 0;
		int bestY = 0;

		for(String prewsizeString : previewSizeValueString.split(","))
		{
			prewsizeString = prewsizeString.trim();

			int dimPosition = prewsizeString.indexOf('x');
			if(dimPosition == -1){
				Log.e(TAG, "Bad prewsizeString:"+prewsizeString);
				continue;
			}

			int newX = 0;
			int newY = 0;

			try{
				newX = Integer.parseInt(prewsizeString.substring(0, dimPosition));
				newY = Integer.parseInt(prewsizeString.substring(dimPosition+1));
			}catch(NumberFormatException e){
				Log.e(TAG, "Bad prewsizeString:"+prewsizeString);
				continue;
			}

			Point screenResolution = new Point(getScreenWH().widthPixels,getScreenWH().heightPixels);

			int newDiff = Math.abs(newX - screenResolution.x)+ Math.abs(newY- screenResolution.y);

			if(newDiff == diff)
			{
				bestX = newX;
				bestY = newY;
				break;
			} else if(newDiff < diff){
				if((3 * newX) == (4 * newY)) {
					bestX = newX;
					bestY = newY;
					diff = newDiff;
				}
			}
		}
		if (bestX > 0 && bestY > 0) {
			return mCamera.new Size(bestX, bestY);
		}
		return null;
	}


	protected DisplayMetrics getScreenWH() {
		DisplayMetrics dMetrics = new DisplayMetrics();
		dMetrics = mContext.getResources().getDisplayMetrics();
		return dMetrics;
	}

}
