package cn.com.artemis.biz.mgb.jshandler;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;

import cn.com.artemis.biz.mgb.MgbApplication;
import cn.com.artemis.biz.mgb.activity.FaceActivity;

public class CallEvant {
    public static final String JS_CALL_CALL = "call";
    private Context mContext;

    public CallEvant(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 打电话
     * @param bridgeWebView
     */
    public  void startCall(BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_CALL, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用原生电话");
                validPermission(data);
            }
        });
    }

    /**
     * 动态权限验证
     */
    private void validPermission(final String tel) {
        Acp.getInstance(mContext).request(new AcpOptions.Builder()
                .setDeniedMessage("需要打开打电话权限")  // 以下为自定义提示语、按钮文字
                /*.setDeniedCloseBtn()
                .setDeniedSettingBtn()
                .setRationalMessage()
                .setRationalBtn()*/
                .setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
            @Override
            public void onGranted() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tel));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
            @Override
            public void onDenied(List<String> permissions) {
                ToastSafeUtils.showLongToast(mContext,"需要打开打电话权限");
            }
        });
    }
}
