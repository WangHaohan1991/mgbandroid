package cn.com.artemis.biz.mgb.face.camera;

import android.net.Uri;

/**
 * Created by whh on 2018/3/2.
 */

public interface CameraCallBack {
    void callBack(String path);
}
