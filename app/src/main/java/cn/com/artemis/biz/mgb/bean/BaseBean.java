package cn.com.artemis.biz.mgb.bean;

import android.text.TextUtils;

public class BaseBean {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        if (TextUtils.isEmpty(message)){
            return "服务器错误，返回数据为空";
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
