package cn.com.artemis.biz.mgb.common;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.cn.artemis.artemisbase.dialog.MenuDialog;
import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.basekit.utils.ListDataManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import cn.com.artemis.biz.mgb.R;

import static android.app.Activity.RESULT_OK;
import static cn.com.artemis.biz.mgb.common.ImgSelectHelper.CAMERA_REQUEST_CODE;
import static cn.com.artemis.biz.mgb.common.ImgSelectHelper.CHOOSE_REQUEST_CODE;
import static cn.com.artemis.biz.mgb.common.ImgSelectHelper.hasFile;

/**
 * Created by whh on 2018/9/5.
 */

public class MgbWebChromeClient extends WebChromeClient {

    private Context mContext;
    View myVideoView;
    View myNormalView;
    public CustomViewCallback callback;
    boolean isFirstLoad = true;
    private ValueCallback<Uri> uploadFile;//定义接受返回值
    private ValueCallback<Uri[]> uploadFiles;
    private  static final int PERMISSION_REQUEST_CODE = 103;
    private ImgSelectHelper mImgSelectHelper;

    public MgbWebChromeClient(Context context) {
        mContext = context;
        mImgSelectHelper = new ImgSelectHelper();
    }

    private void getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED  ) {
                ((Activity)mContext).requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            } else {
                goToGetImg();
            }
        } else {
            goToGetImg();
        }
    }
    private void openPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED  ) {
                ((Activity)mContext).requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    List<String> deniedPermissions = new ArrayList<>();
                    for (int i = 0; i < grantResults.length; i++) {
                        int grantResult = grantResults[i];
                        String permission = permissions[i];
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            deniedPermissions.add(permission);
                        }
                    }
                    //被拒绝权限
                    if (deniedPermissions.isEmpty()) {
                        goToGetImg();
                    } else {
                        ToastSafeUtils.showLongToast(mContext, "请前往权限管理开启相机和相册相关权限");
                    }
                }
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onPermissionRequest(PermissionRequest request) {
        //                super.onPermissionRequest(request);//必须要注视掉
        request.grant(request.getResources());
        request.getOrigin();
        openPermission();
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        result.confirm();
        return true;
    }

    @Override
    public void onProgressChanged(WebView webView, int newProgress) {
        if (newProgress >= 100) {
            if (isFirstLoad){
                isFirstLoad = false;
                if(mOnLoadingCompeleteCallback!=null){
                    mOnLoadingCompeleteCallback.loadSuccess();
                }
            }
        }
    }
    /**
     * 全屏播放配置
     */
    @Override
    public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
        FrameLayout normalView = (FrameLayout) ((Activity)mContext).findViewById(android.R.id.content);
        ViewGroup viewGroup = (ViewGroup) normalView.getParent();
        viewGroup.removeView(normalView);
        // 设置背景色为黑色
        view.setBackgroundColor(mContext.getResources().getColor(R.color.black));
        viewGroup.addView(view);
        myVideoView = view;
        myNormalView = normalView;
        callback = customViewCallback;
        ((Activity)mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
    @Override
    public void onHideCustomView() {
        hideCustomView();
    }

    public void hideCustomView() {
        try {
            if (callback != null) {
                callback.onCustomViewHidden();
                callback = null;
            }
            if (myVideoView != null) {
                ViewGroup viewGroup = (ViewGroup) myVideoView.getParent();
                viewGroup.removeView(myVideoView);
                viewGroup.addView(myNormalView);
            }
            ((Activity)mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // For Android 3.0+
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
        this.uploadFile = uploadFile;
        openFileChooseProcess();
    }

    // For Android < 3.0
    public void openFileChooser(ValueCallback<Uri> uploadMsgs) {
        this.uploadFile = uploadFile;
        openFileChooseProcess();
    }

    // For Android  > 4.1.1
//    @Override
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        this.uploadFile = uploadFile;
        openFileChooseProcess();
    }

    // For Android  >= 5.0
    @Override
    public boolean onShowFileChooser(WebView webView,
                                     ValueCallback<Uri[]> filePathCallback,
                                     FileChooserParams fileChooserParams) {
        this.uploadFiles = filePathCallback;
        openFileChooseProcess();
        return true;
    }

    private void openFileChooseProcess() {
        getPermission();
    }

    private void goToGetImg() {
        MenuDialog menuDialog = new MenuDialog(mContext,
                ListDataManager.getListData(mContext, R.array.imgArray),
                new MenuDialog.MenuDialogOnButtonClickListener() {
                    @Override
                    public void onButtonClick(String name, int index) {
                        if (index==0){
                            mImgSelectHelper.openAlbum(mContext);
                        }else if(index == 1){
                            mImgSelectHelper.openCamera(mContext);
                        }else {
                            if (uploadFiles != null) {
                                uploadFiles.onReceiveValue(null);
                                uploadFiles = null;
                            }
                            if (uploadFile != null) {
                                uploadFile.onReceiveValue(null);
                                uploadFile = null;
                            }
                        }
                    }
                });
        menuDialog.setItemColor(R.color.colorAccent);
        menuDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null == uploadFile && null == uploadFiles) return;
        if (resultCode != RESULT_OK) {//同上所说需要回调onReceiveValue方法防止下次无法响应js方法
            if (uploadFiles != null) {
                uploadFiles.onReceiveValue(null);
                uploadFiles = null;
            }
            if (uploadFile != null) {
                uploadFile.onReceiveValue(null);
                uploadFile = null;
            }
            return;
        }
        Uri result = null;
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (null != data && null != data.getData()) {
                result = data.getData();
            }
            if (mImgSelectHelper.isAndroidQ) {
                // Android 10 使用图片uri加载
                result = mImgSelectHelper.mCameraUri;
            } else {
                // 使用图片路径加载
                if (result == null && hasFile(mImgSelectHelper.mCameraImagePath)) {
                    result = Uri.fromFile(new File(mImgSelectHelper.mCameraImagePath));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                        //通过FileProvider创建一个content类型的Uri
                        result = FileProvider.getUriForFile(mContext, mContext.getPackageName()
                                + ".fileprovider", new File(mImgSelectHelper.mCameraImagePath));
                    }
                }
            }
            if (uploadFiles != null) {
                uploadFiles.onReceiveValue(new Uri[]{result});
                uploadFiles = null;
            } else if (uploadFile != null) {
                uploadFile.onReceiveValue(result);
                uploadFile = null;
            }

        } else if (requestCode == CHOOSE_REQUEST_CODE) {
            if (data != null) {
                result = data.getData();
            }
            if (uploadFiles != null) {
                onActivityResultAboveL(data);
            } else if (uploadFile != null) {
                uploadFile.onReceiveValue(result);
                uploadFile = null;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onActivityResultAboveL(Intent intent) {
        Uri[] results = null;
        if (intent != null) {
            String dataString = intent.getDataString();
            ClipData clipData = intent.getClipData();
            if (clipData != null) {
                results = new Uri[clipData.getItemCount()];
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);
                    results[i] = item.getUri();
                }
            }
            if (dataString != null)
                results = new Uri[]{Uri.parse(dataString)};
        }
        uploadFiles.onReceiveValue(results);
        uploadFiles = null;
    }
    private OnLoadingCompeleteCallback mOnLoadingCompeleteCallback;

    public void setOnLoadingCompeleteCallback(OnLoadingCompeleteCallback onLoadingCompeleteCallback) {
        mOnLoadingCompeleteCallback = onLoadingCompeleteCallback;
    }

    public interface OnLoadingCompeleteCallback{
        void loadSuccess();
    }
}
