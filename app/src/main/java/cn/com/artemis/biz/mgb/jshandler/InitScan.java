package cn.com.artemis.biz.mgb.jshandler;

import android.Manifest;
import android.content.Context;
import android.content.Intent;

import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;



public class InitScan {
    public static final String JS_CALL_SCAN = "scan";
    private Context mContext;

    public InitScan(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 扫描相关
     * @param bridgeWebView
     */
    public  void startScan(BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_SCAN, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用原生扫描");
                validPermission();
            }
        });
    }

    /**
     * 动态权限验证
     */
    private void validPermission() {
        Acp.getInstance(mContext).request(new AcpOptions.Builder()
                .setDeniedMessage("扫描二维码需要打开相机和散光灯的权限")  // 以下为自定义提示语、按钮文字
                /*.setDeniedCloseBtn()
                .setDeniedSettingBtn()
                .setRationalMessage()
                .setRationalBtn()*/
                .setPermissions(Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE).build(), new AcpListener() {
            @Override
            public void onGranted() {
            }

            @Override
            public void onDenied(List<String> permissions) {
                ToastSafeUtils.showLongToast(mContext,"扫描二维码需要打开相机和散光灯的权限");
            }
        });
    }

}
