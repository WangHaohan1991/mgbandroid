package cn.com.artemis.biz.mgb.jshandler;

import android.content.Context;

import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.cn.artemis.basekit.utils.SharedPreferencesUtil;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import cn.com.artemis.biz.mgb.jshandler.bean.UserInfoBean;


/**
 * Created by whh on 2018/8/31.
 */

public class LoginRelevant {


    public static final String JS_CALL_LOGIN= "loginSuccess";
    public static final String JS_CALL_LOGOUT= "logout";
    /**
     * 登录相关
     * @param context
     * @param bridgeWebView
     */
    public  void loginRelevant(final Context context, BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_LOGIN, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                //登录成功以后客户端保存数据
                //云信登录
                LogUtils.e("TGA","...登录成功，缓存数据");
                LogUtils.i("TGA",data);
                SharedPreferencesUtil.saveUserInfo(context,data);
                UserInfoBean userInfoBean = JsonHelper.parseObject(data,UserInfoBean.class);
            }
        });
        bridgeWebView.registerHandler(JS_CALL_LOGOUT, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                //云信注销
                LogUtils.e("TGA","...退出登录成功，清除缓存数据");
                SharedPreferencesUtil.clear(context);
            }
        });
    }
}
