package cn.com.artemis.biz.mgb.jshandler;

import android.content.Context;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.basekit.thirdpay.bean.AliPayOrderBean;
import com.cn.artemis.basekit.thirdpay.bean.PaypalSuperBean;
import com.cn.artemis.basekit.thirdpay.bean.WechatOrderBean;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.cn.artemis.basekit.thirdpay.interfaces.OnPayResultListener;
import com.cn.artemis.basekit.thirdpay.pay.ThirdPayManager;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by whh on 2018/8/2.
 */

public class ThirdPay {
    public static final String JS_CALL_GOPAY= "goPay";
    public static final String PAY_RESULT_SUCCESS = "payResultSuccess";
    public static final String PAY_RESULT_FAILED= "payResultFailed";
    public static final String PAY_RESULT_CANCLE = "payResultCancle";
    private ThirdPayManager mPayManager;
    private Context mContext;

    public ThirdPay(Context context) {
        mContext = context;
        BusHelper.registe(this);
        initThirdPay();
    }
    /**
     * 三方支付
     * @param bridgeWebView
     */
    public  void goPay(final BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_GOPAY, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                payByType(data);
            }
        });
    }

    /**
     * 初始化三方支付
     */
    private void initThirdPay() {
        mPayManager = new ThirdPayManager(mContext);
        mPayManager.setOnPayResultListener(new OnPayResultListener() {
            @Override
            public void paySuccess() {
                sendPayResult(ThirdPay.PAY_RESULT_SUCCESS);
            }

            @Override
            public void payFailed() {
                sendPayResult(ThirdPay.PAY_RESULT_FAILED);
            }

            @Override
            public void payCancel() {
                sendPayResult(ThirdPay.PAY_RESULT_CANCLE);
            }
        });
    }

    private void sendPayResult(String methodName) {
        CallH5Event event = new CallH5Event();
        event.methodName=methodName;
        event.param="";
        BusHelper.postEvent(event);
    }

    /**
     *选择类型支付
     * @param string
     */
    private  void payByType(String string) {
        try {
            JSONObject jsonObject =new JSONObject(string);
            int payType = jsonObject.getInt("payChanel");
            if (payType == ThirdPayManager.ALIPAY){
                AliPayOrderBean dataBean = JsonHelper.parseObject(string, AliPayOrderBean.class);
                mPayManager.payByAliDirectly(dataBean.getData());
            }else if (payType == ThirdPayManager.WXPAY){
                WechatOrderBean dataBean = JsonHelper.parseObject(string, WechatOrderBean.class);
                mPayManager.payByWechatDirectly(dataBean.getData());
            }else if (payType == ThirdPayManager.PAYPAL){
                PaypalSuperBean payPalOrderBean = JsonHelper.parseObject(string, PaypalSuperBean.class);
                mPayManager.payByPayPalDirectly(payPalOrderBean.getData());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void release() {
        BusHelper.unRegiste(this);
    }
}
