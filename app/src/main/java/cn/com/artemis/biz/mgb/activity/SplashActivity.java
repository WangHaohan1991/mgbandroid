package cn.com.artemis.biz.mgb.activity;

import android.content.Intent;

import com.cn.artemis.artemisbase.base.BaseActivity;

import java.util.Timer;
import java.util.TimerTask;

import cn.com.artemis.biz.mgb.BuildConfig;
import cn.com.artemis.biz.mgb.R;
import cn.com.artemis.biz.mgb.common.StatusBarUtil;


/**
 * 启动页面
 */
public class SplashActivity extends BaseActivity {

    public static final int MODE_DEFAULT = 0;

    public static final int MODE_SONIC = 1;

    public static final int MODE_SONIC_WITH_OFFLINE_CACHE = 2;

    private static final int PERMISSION_REQUEST_CODE_STORAGE = 1;

//    private final String H5_DEBUG_URL = "http://192.168.2.6:8080";
//    private final String H5_RELEASE_URL = "http://192.168.2.6:8080";
    private final String H5_DEBUG_URL = "file:///android_asset/index.html";
    private final String H5_RELEASE_URL = "file:///android_asset/index.html";
    private String H5_URL = H5_DEBUG_URL;
    private Timer mTimer;
    private TimerTask mTimerTask;

    @Override
    protected int getContentLayout() {
        return R.layout.splash_activity;
    }

    @Override
    protected void initView() {
        //处理首次安装点击打开切到后台,点击桌面图标再回来重启的问题及通过应用宝唤起在特定条件下重走逻辑的问题
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        }
        if(!BuildConfig.DEBUG){
            H5_URL = H5_RELEASE_URL;
        }
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                goWebView();
            }
        };
        mTimer = new Timer();
        mTimer.schedule(mTimerTask,2*1000);
    }

    /**
     * 跳转到h5
     */
    private void goWebView() {
        Intent intent = new Intent(this, MgbActivity.class);
        intent.putExtra(MgbActivity.PARAM_URL, H5_URL);
        intent.putExtra(MgbActivity.PARAM_MODE, MODE_SONIC_WITH_OFFLINE_CACHE);
        startActivityForResult(intent, -1);
        finish();
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();
        hidetoolBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
        }
    }
}
