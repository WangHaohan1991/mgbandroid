package cn.com.artemis.biz.mgb.bean;
/**
 * @Description:
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/5/24 15:19
 * @version V1.0
 */

import android.text.TextUtils;

import java.util.List;

/**
 * @ClassName: FileUploadBaen
 *Description: @author android移动客户端-王浩韩
 * @date 2017/5/24 15:19
 */

public class FileUploadBaen extends  BaseBean{



    /**
     * code : 200
     * data : ["user/icon/1556997201966662666.png"]
     */
    private List<String> data;


    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
