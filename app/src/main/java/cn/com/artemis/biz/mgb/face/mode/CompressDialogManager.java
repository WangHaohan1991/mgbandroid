package cn.com.artemis.biz.mgb.face.mode;

import android.app.Activity;

import cn.com.artemis.biz.mgb.face.ui.PictureDialog;


/**
 * Created by whh on 2018/4/2.
 */



public class CompressDialogManager {

    protected PictureDialog dialog;
    protected PictureDialog compressDialog;

    private Activity mActivity;

    public CompressDialogManager(Activity activity) {
        mActivity = activity;
    }

    /**
     * loading dialog
     */
    public void showPleaseDialog() {
        if (!mActivity.isFinishing()) {
            dismissDialog();
            dialog = new PictureDialog(mActivity);
            dialog.show();
        }
    }

    /**
     * dismiss dialog
     */
    public void dismissDialog() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * compress loading dialog
     */
    public void showCompressDialog() {
        if (!mActivity.isFinishing()) {
            dismissCompressDialog();
            compressDialog = new PictureDialog(mActivity);
            compressDialog.show();
        }
    }

    /**
     * dismiss compress dialog
     */
    public void dismissCompressDialog() {
        try {
            if (!mActivity.isFinishing()
                    && compressDialog != null
                    && compressDialog.isShowing()) {
                compressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}




