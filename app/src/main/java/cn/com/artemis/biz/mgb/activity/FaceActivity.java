package cn.com.artemis.biz.mgb.activity;



import android.app.Activity;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cn.artemis.artemisbase.base.BaseActivity;
import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;


import cn.com.artemis.biz.mgb.R;
import cn.com.artemis.biz.mgb.common.FaceNetworkModule;
import cn.com.artemis.biz.mgb.common.StatusBarUtil;
import cn.com.artemis.biz.mgb.face.camera.CameraCallBack;
import cn.com.artemis.biz.mgb.face.camera.CameraInterface;
import cn.com.artemis.biz.mgb.face.camera.CameraSurfaceView;
import cn.com.artemis.biz.mgb.face.mode.CompressDialogManager;
import cn.com.artemis.biz.mgb.face.mode.GoogleFaceDetect;
import cn.com.artemis.biz.mgb.face.ui.FaceView;
import cn.com.artemis.biz.mgb.face.util.DisplayUtil;
import cn.com.artemis.biz.mgb.face.util.EventUtil;
import cn.com.artemis.biz.mgb.interfaces.Callback;

public class FaceActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "FaceActivity";
    private static final String NATIVE_CALL_JS_FACE = "nativeCallJsFace";
    private static final String USER_AVATAR = "user/avatar/";

    private CameraSurfaceView cameraSurfaceView;
    private FaceView faceView;
    private ImageView switchCamera;

    private GoogleFaceDetect googleFaceDetect;

    private MainHandler mainHandler;

    private float previewRate = -1f;

    private boolean isFont = true; // 默认前置摄像

    private CompressDialogManager manager;

    @Override
    protected void initToolBar() {
        super.initToolBar();
        hidetoolBar();
        StatusBarUtil.setTranslucentStatus((Activity) mContext);
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_face;
    }

    @Override
    protected void initView() {
        initFindViewById();
//        initViewParams();
        mainHandler = new MainHandler();
        manager = new CompressDialogManager((Activity) mContext);
        googleFaceDetect = new GoogleFaceDetect(mContext, mainHandler);
        mainHandler.sendEmptyMessageDelayed(EventUtil.CAMERA_HAS_STARTED_PREVIEW, 1500);
        switchCamera.setOnClickListener(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        faceView.release();
    }

    private void initFindViewById () {
        cameraSurfaceView = findViewById(R.id.camera_surfaceview);
        faceView = findViewById(R.id.face_view);
        switchCamera = findViewById(R.id.switch_camera);
    }

    private void initViewParams() {
        ViewGroup.LayoutParams params = cameraSurfaceView.getLayoutParams();
        Point p = DisplayUtil.getScreenMetrics(mContext);
        params.width = p.x;
        params.height = p.y;
        previewRate = DisplayUtil.getScreenRate(mContext); //默认全屏的比例预览
        cameraSurfaceView.setLayoutParams(params);
    }

    private void startGoogleFaceDetect() {
        try {
            Camera.Parameters params = CameraInterface.getInstance(mContext).getCameraParams();
            if (params != null && params.getMaxNumDetectedFaces() > 0) {
                if (faceView != null) {
                    faceView.clearFaces();
                    faceView.setVisibility(View.VISIBLE);
                }
                CameraInterface.getInstance(mContext).getCameraDevice().setFaceDetectionListener(googleFaceDetect);
                CameraInterface.getInstance(mContext).getCameraDevice().startFaceDetection();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stopGoogleFaceDetect() {
        Camera.Parameters params = CameraInterface.getInstance(mContext).getCameraParams();
        if (params.getMaxNumDetectedFaces() > 0) {
            CameraInterface.getInstance(mContext).getCameraDevice().setFaceDetectionListener(null);
            CameraInterface.getInstance(mContext).getCameraDevice().stopFaceDetection();
            faceView.clearFaces();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.switch_camera: // 切换摄像头
                switchCamera();
                break;
        }
    }

    private class MainHandler extends Handler {
        @Override
        public void handleMessage( Message msg) {
            switch (msg.what) {
                case EventUtil.UPDATE_FACE_RECT:
                    Camera.Face[] faces = (Camera.Face[]) msg.obj;
                    faceView.setFaces(faces);
                    faceView.setCenterFaceCallBack(new FaceView.CenterFaceCallBack() {
                        @Override
                        public void callBack() {
                            startCamera();
                        }
                    });
                    break;
                case EventUtil.CAMERA_HAS_STARTED_PREVIEW:
                    startGoogleFaceDetect();
                    break;
            }
            super.handleMessage(msg);
        }
    }

    /**
     * 切换前后摄像头
     */
    private void switchCamera() {
        stopGoogleFaceDetect();
        int newId = (CameraInterface.getInstance(mContext).getCameraId() + 1) % 2;
        if ( newId == 0) {
            // 后置摄像头
            isFont = false;
            ToastSafeUtils.showLongToast(mContext,"切换到后置摄像头");
        } else {
            // 前置摄像头
            isFont = true;
            ToastSafeUtils.showLongToast(mContext,"切换到前置摄像头");
        }
        faceView.setFont(isFont);
        CameraInterface.getInstance(mContext).doStopCamera();
        CameraInterface.getInstance(mContext).doOpenCamera(null, newId);
        CameraInterface.getInstance(mContext).doStartPreview(cameraSurfaceView.getSurfaceHolder(), previewRate);
        mainHandler.sendEmptyMessageDelayed(EventUtil.CAMERA_HAS_STARTED_PREVIEW, 1500);
    }
    /**
     * 开始拍照
     */
    private void startCamera () {
        manager.showPleaseDialog();
        stopGoogleFaceDetect();
        CameraInterface.getInstance(mContext).doTakePicture(new CameraCallBack() {
            @Override
            public void callBack(final String path) {
                if(path != null) {
                    // 上传图片
                    uploadFacePicture(path);
                } else {
                    manager.dismissDialog();
                    startGoogleFaceDetect();
                }
            }
        });
    }

    /**
     * 上传人脸验证图片
     */
    private void uploadFacePicture (String path) {
        FaceNetworkModule.postSingleFileUpload(mContext, path, USER_AVATAR, new Callback<String>() {
            @Override
            public void onSuccess(String s) {
                postValidFace(s);
            }

            @Override
            public void onFailed(String msg) {
                ToastSafeUtils.showShortToast(mContext,msg);
                manager.dismissDialog();
                startGoogleFaceDetect();
            }
        });
    }

    /**
     * 人脸验证
     */
    private void postValidFace (String pic) {
        FaceNetworkModule.postFaceDetect(mContext, pic, new Callback<String>() {
            @Override
            public void onSuccess(String s) {
                validFaceResult();
            }

            @Override
            public void onFailed(String msg) {
                ToastSafeUtils.showShortToast(mContext,msg);
                manager.dismissDialog();
                startGoogleFaceDetect();
            }
        });
    }

    /**
     * 返回验证结果
     */
    private void validFaceResult () {
        manager.dismissDialog();
        CallH5Event event = new CallH5Event();
        event.methodName= NATIVE_CALL_JS_FACE;
        event.param= "";
        BusHelper.postEvent(event);
        onBackPressedSupport();
    }
}
