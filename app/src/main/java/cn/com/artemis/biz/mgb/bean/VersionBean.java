package cn.com.artemis.biz.mgb.bean;

public class VersionBean {
    /**
     * appId : 2
     * context : 1、施工日志新增劳务单位选择
     2、民工工资添加劳务单位审批
     * downloadPublicUrl : http://118.24.120.58/app/mgb.apk
     * id : 1
     * insertTime : 2018-08-21 10:41:50
     * type : 1
     * updateTime : 2018-08-21 10:40:57
     * version : 1.0.4
     */

    private int appId;
    private String context;
    private String downloadPublicUrl;
    private int id;
    private String insertTime;
    private int type;
    private String updateTime;
    private String version;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getDownloadPublicUrl() {
        return downloadPublicUrl;
    }

    public void setDownloadPublicUrl(String downloadPublicUrl) {
        this.downloadPublicUrl = downloadPublicUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(String insertTime) {
        this.insertTime = insertTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
