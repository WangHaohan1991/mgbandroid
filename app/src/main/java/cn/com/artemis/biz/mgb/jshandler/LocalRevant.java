package cn.com.artemis.biz.mgb.jshandler;

import android.Manifest;
import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;

import cn.com.artemis.biz.mgb.bean.LocationBean;
import cn.com.artemis.biz.mgb.common.LocationProvider;


public class LocalRevant implements LocationProvider.LocationGetListener{
    public static final String JS_CALL_LOCATION = "location";
    public static final String LOCATION_RESULT = "nativeCallJsLocation";
    private Context mContext;

    public LocalRevant(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 获取位置
     * @param bridgeWebView
     */
    public  void startLocaiton(BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_LOCATION, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...获取位置");
                validPermission();
            }
        });
    }

    /**
     * 动态权限验证
     */
    private void validPermission() {
        Acp.getInstance(mContext).request(new AcpOptions.Builder()
                .setDeniedMessage("获取定位权限")  // 以下为自定义提示语、按钮文字
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION).build(), new AcpListener() {
            @Override
            public void onGranted() {
                LocationProvider locationProvider = new LocationProvider(mContext);
                locationProvider.initLocation();
                locationProvider.setAMapLocationListener(LocalRevant.this);
            }

            @Override
            public void onDenied(List<String> permissions) {
                ToastSafeUtils.showLongToast(mContext,"需要打开定位权限！");
            }
        });
    }

    @Override
    public void getLocationInfo(AMapLocation aMapLocation) {
        LocationBean locationBean = new LocationBean();
        locationBean.setLatitude(aMapLocation.getLatitude() + "");
        locationBean.setLongitude(aMapLocation.getLongitude() + "");
        locationBean.setAddress(aMapLocation.getAddress());
        CallH5Event event = new CallH5Event();
        event.methodName = LOCATION_RESULT;
        event.param = JsonHelper.toJSONString(locationBean);
        BusHelper.postEvent(event);
    }
}
