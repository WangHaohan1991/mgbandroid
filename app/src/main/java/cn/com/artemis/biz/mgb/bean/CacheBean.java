package cn.com.artemis.biz.mgb.bean;

public class CacheBean {

    /**
     * cacheText : “0KB”
     */

    private String cacheText;

    public String getCacheText() {
        return cacheText;
    }

    public void setCacheText(String cacheText) {
        this.cacheText = cacheText;
    }
}
