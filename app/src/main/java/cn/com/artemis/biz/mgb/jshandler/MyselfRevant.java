package cn.com.artemis.biz.mgb.jshandler;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import cn.com.artemis.biz.mgb.bean.CacheBean;
import cn.com.artemis.biz.mgb.common.CacheUtil;


public class MyselfRevant {

    public static final String JS_CALL_CLEARCACHE= "clearCache";
    public static final String JS_CALL_GETCACHE= "getCache";
    public static final String NATIVE_CALL_JS_CACHE = "nativeCallJsCache";
    private Context mContext;

    public MyselfRevant(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 个人中心相关
     * @param bridgeWebView
     */
    public  void myselfRevant(final BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_GETCACHE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用获取缓存成功");
                bridgeWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        callJsHandler.sendEmptyMessage(-1);
                    }
                });
            }
        });
        bridgeWebView.registerHandler(JS_CALL_CLEARCACHE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用清除缓存成功");
                bridgeWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        CacheUtil.clearAllCache(mContext);
                        callJsHandler.sendEmptyMessage(-1);
                    }
                });
            }
        });
    }

    private Handler callJsHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == -1){
                try {
                    String cache = CacheUtil.getTotalCacheSize(mContext);
                    CacheBean cacheBean = new CacheBean();
                    cacheBean.setCacheText(cache);
                    CallH5Event event = new CallH5Event();
                    event.methodName= NATIVE_CALL_JS_CACHE;
                    event.param= JsonHelper.toJSONString(cacheBean);
                    BusHelper.postEvent(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
