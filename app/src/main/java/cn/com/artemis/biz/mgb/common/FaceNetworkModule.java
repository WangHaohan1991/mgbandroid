package cn.com.artemis.biz.mgb.common;

import android.content.Context;
import android.util.Pair;

import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.network.net.NetManager;
import com.cn.artemis.artemisbase.network.net.callbacks.RequestCallback;
import com.cn.artemis.artemisbase.network.okgo.OkGo;
import com.cn.artemis.artemisbase.network.okgo.request.PostRequest;
import com.cn.artemis.artemisbase.utils.other.AppUtils;
import com.cn.artemis.artemisbase.utils.other.DeviceUtils;
import com.cn.artemis.artemisbase.utils.other.NetworkUtils;
import com.cn.artemis.basekit.utils.PictureUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import cn.com.artemis.biz.mgb.BuildConfig;
import cn.com.artemis.biz.mgb.MgbApplication;
import cn.com.artemis.biz.mgb.bean.BaseBean;
import cn.com.artemis.biz.mgb.bean.FileUploadBaen;
import cn.com.artemis.biz.mgb.face.util.FileUtil;
import cn.com.artemis.biz.mgb.interfaces.Callback;

public class FaceNetworkModule {

    private static final String URL_IMG_UPDATE = "http://mgbapi.y7yun.com:1024/file/upload";
    private static final String AUTH_FACE_DETECT = BuildConfig.DEBUG ? "http://118.24.120.254:6480/auth/face/verify" : "http://mmmgbapi.y7yun.com/auth/face/verify";
//    private static final String AUTH_FACE_DETECT = "http://192.168.4.16:2980/auth/face/verify";

    public static void postSingleFileUpload (Context context, String url, String fileType, Callback<String> callback) {
        Pair<String, File> pair = getCompressPic(url);
        if (pair.second.exists()) {
            //图片压缩上传
            upload(context, pair.first, fileType, callback);
        } else {
            //直接上传
            upload(context, url, fileType, callback);
        }
    }
    public static void postFaceDetect (final Context context, String pic, final Callback<String> callback) {
        PostRequest request = generateRequest(context,pic);
        NetManager.postWithCallback(request, false, new RequestCallback<String>() {
            @Override
            public void onSuccess(String param) {
                BaseBean baseBean = JsonHelper.parseObject(param, BaseBean.class);
                if (baseBean.getCode()== 200) {
                    //更新头像
                    callback.onSuccess("人脸识别成功！");
                } else {
                    callback.onFailed("人脸识别失败！");
                }
            }

            @Override
            public void onFailed(String message) {
                callback.onFailed(message);
            }
        });
    }
    private static void upload(final Context context, String url, String fileType, final Callback<String> callback) {
        File file = new File(url);
        PostRequest request = generateRequest(context,fileType, file);
        NetManager.postWithCallback(request, false, new RequestCallback<String>() {
            @Override
            public void onSuccess(String param) {
                FileUploadBaen fileUploadBaen = JsonHelper.parseObject(param, FileUploadBaen.class);
                if (fileUploadBaen.getCode()== 200) {
                    //更新头像
                    String id = fileUploadBaen.getData().get(0);
                    callback.onSuccess(id);
                } else {
                    callback.onFailed("上传失败");
                }
            }

            @Override
            public void onFailed(String message) {
                callback.onFailed(message);
            }
        });
    }

    private static PostRequest generateRequest(Context context, String fileType, File file){
        PostRequest request = OkGo.post(URL_IMG_UPDATE);
        request = addHeaders(context,request);
        request = request.headers("token", MgbApplication.token)
                .headers("fileType", fileType)
                .params("file", file);
        return request;
    }
    private static PostRequest generateRequest(Context context, String url){
        JSONObject json = new JSONObject();
        try {
            json.put("faceImg",url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PostRequest request = OkGo.post(AUTH_FACE_DETECT);
        request = addHeaders(context,request);
        request = request.headers("token", MgbApplication.token)
                .upJson(json.toString());
        return request;
    }
    private static Pair<String, File> getCompressPic(String url) {
        File tempPic = new File(url);
        String pic_path = tempPic.getPath();
        String targetPath = FileUtil.createTargetPath();
        //调用压缩图片的方法，返回压缩后的图片path
        String compressImage = PictureUtil.compressImage(pic_path, targetPath, 80);
        File compressedPic = new File(compressImage);
        return new Pair(compressImage, compressedPic);
    }

    private static PostRequest addHeaders(Context context,PostRequest postRequest) {
        return postRequest.headers("Accept-Language", "zh_CN")
                .headers("appId", "2")//appId
                .headers("deviceIdentifier", DeviceUtils.getAndroidID(context))//设备唯一标识
                .headers("deviceName", DeviceUtils.getManufacturer())//设备名
                .headers("deviceType", DeviceUtils.getModel())//设备类型
                .headers("osType", "Android")//操作系统类型
                .headers("osVersion", DeviceUtils.getSDKVersion()+"")//操作系统版本
                .headers("network", NetworkUtils.getNetworkType(context).toString())//网络环境
                .headers("appVersion", AppUtils.getAppVersionName(context));
    }
}
