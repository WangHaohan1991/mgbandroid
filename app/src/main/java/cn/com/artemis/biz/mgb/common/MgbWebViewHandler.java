package cn.com.artemis.biz.mgb.common;

import android.content.Context;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.cn.atemis.artemisbwv.BaseWebviewHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.hwangjr.rxbus.annotation.Subscribe;

import cn.com.artemis.biz.mgb.jshandler.CallEvant;
import cn.com.artemis.biz.mgb.jshandler.FaceRevant;
import cn.com.artemis.biz.mgb.jshandler.InitScan;
import cn.com.artemis.biz.mgb.jshandler.LocalRevant;
import cn.com.artemis.biz.mgb.jshandler.LoginRelevant;
import cn.com.artemis.biz.mgb.jshandler.MyselfRevant;
import cn.com.artemis.biz.mgb.jshandler.ThirdPay;
import cn.com.artemis.biz.mgb.jshandler.UpdateEvant;


/**
 * Created by whh on 2018/5/25.
 */

public class MgbWebViewHandler extends BaseWebviewHandler {

    private ThirdPay mThirdPay;
    public MgbWebViewHandler(Context mContext, BridgeWebView mBridgeWebView) {
        super(mContext, mBridgeWebView);
        BusHelper.registe(this);
    }

    @Override
    protected void init() {
        //初始化
        //JS调用原生三方支付
        mThirdPay = new ThirdPay(mContext);
        mThirdPay.goPay(mBridgeWebView);
        //登录模块
        new LoginRelevant().loginRelevant(mContext,mBridgeWebView);
        //我的模块
        new MyselfRevant(mContext).myselfRevant(mBridgeWebView);
        //扫描模块
        new InitScan(mContext).startScan(mBridgeWebView);
        //定位模块
        new LocalRevant(mContext).startLocaiton(mBridgeWebView);
        //人脸识别模块
        new FaceRevant(mContext).startFace(mBridgeWebView);
        //拨打电话模块
        new CallEvant(mContext).startCall(mBridgeWebView);
        //版本更新模块
        new UpdateEvant(mContext).initUpdate(mBridgeWebView);
    }

    @Subscribe
    public void callH5(CallH5Event event){
        mBridgeWebView.loadUrl(getLoadUrl(event.methodName,event.param));
    }

    @Override
    public void release() {
        BusHelper.unRegiste(this);
        if (mThirdPay!=null)
            mThirdPay.release();
    }
}
