package cn.com.artemis.biz.mgb.jshandler.bean;

/**
 * Created by whh on 2018/9/1.
 */

public class CallJsBean {
    private String language;
    private UserInfoBean userInfo;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public UserInfoBean getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoBean userInfo) {
        this.userInfo = userInfo;
    }
}
