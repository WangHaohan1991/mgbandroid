package cn.com.artemis.biz.mgb.face.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Face;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;


import com.cn.artemis.artemisbase.utils.other.LogUtils;

import java.util.Timer;
import java.util.TimerTask;

import cn.com.artemis.biz.mgb.face.camera.CameraInterface;
import cn.com.artemis.biz.mgb.face.mode.CompressDialogManager;
import cn.com.artemis.biz.mgb.face.mode.GoogleFaceDetect;
import cn.com.artemis.biz.mgb.face.util.Util;


public class FaceView extends View {
	private static final String TAG = "whh";
	private Context mContext;
	private Paint mLinePaint;
	private Paint mTextPaint;
	private Face[] mFaces;
	private Matrix mMatrix = new Matrix();
	private RectF mRect = new RectF();
	private Rect mMaxFontRect = new Rect(-263,-611,684,610);// 最大范围
	private Rect mMaxBackRect = new Rect(-747,-725,346,685);// 最大范围
	private boolean isFont = true;
	String textString = "请尽量保持身体与人形区域重叠！";
	private Timer mTimer;
	private TimerTask mTimerTask;
	private CenterFaceCallBack mCenterFaceCallBack;
	public FaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initPaint();
		initTipsPaint();
		mContext = context;
	}


	public void setFont(boolean font) {
		isFont = font;
	}

	public void setFaces(Face[] faces){
		this.mFaces = faces;
		invalidate();
	}
	public void clearFaces(){
		mFaces = null;
		invalidate();
	}

	public void setCenterFaceCallBack(CenterFaceCallBack centerFaceCallBack) {
		mCenterFaceCallBack = centerFaceCallBack;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		if(mFaces == null || mFaces.length < 1){
			return;
		}
		boolean isMirror = false;
		int Id = CameraInterface.getInstance(mContext).getCameraId();
		if(Id == CameraInfo.CAMERA_FACING_BACK){
			isMirror = false; //后置Camera无需mirror
		}else if(Id == CameraInfo.CAMERA_FACING_FRONT){
			isMirror = true;  //前置Camera需要mirror
		}
		Util.prepareMatrix(mMatrix, isMirror, 90, getWidth(), getHeight());
		canvas.save();
		mMatrix.postRotate(0); //Matrix.postRotate默认是顺时针
		canvas.rotate(-0);   //Canvas.rotate()默认是逆时针
		for(int i = 0; i< mFaces.length; i++){
			mRect.set(mFaces[i].rect);
			mMatrix.mapRect(mRect);
			canvas.drawRect(mRect, mLinePaint);
			Rect bounds = new Rect();
			mTextPaint.getTextBounds(textString, 0, textString.length(), bounds);
			int textWidth = bounds.right-bounds.left;
			int baseX = (getWidth() - textWidth)/2;
			int baseY = (int) (getHeight() * 0.9);
			canvas.drawText(textString,baseX,baseY,mTextPaint);
			setCenterFace(mFaces[i].rect);
		}
		canvas.restore();
		super.onDraw(canvas);
	}

	private void setCenterFace(final Rect rect) {
		post(new Runnable() {
			@Override
			public void run() {
				if (comparaRect(isFont ? mMaxFontRect : mMaxBackRect,rect)){
					startTimer();
				} else {
					release();
					textString = "请尽量保持身体与人形区域重叠！";
				}
			}
		});
	}

	private void initPaint(){
		mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		int color = Color.rgb(255, 255, 255);
//		mLinePaint.setColor(Color.RED);
		mLinePaint.setColor(color);
		mLinePaint.setStyle(Style.STROKE);
		mLinePaint.setStrokeWidth(5f);
		mLinePaint.setAlpha(180);
		mLinePaint.setTextSize(50);
	}
	private void initTipsPaint(){
		mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		int color = Color.rgb(255, 255, 255);
		mTextPaint.setColor(color);
		mTextPaint.setStyle(Style.FILL);
		mTextPaint.setStrokeWidth(3);
		mTextPaint.setTextSize(46);
	}


	private void startTimer () {
		textString = "获取人脸照片中，请保持当前姿势...";
		if(mTimer == null) {
			mTimer = new Timer();
		}
		if (mTimerTask == null) {
			mTimerTask = new TimerTask() {
				@Override
				public void run() {
					post(new Runnable() {
						@Override
						public void run() {
							if (mCenterFaceCallBack != null)
								mCenterFaceCallBack.callBack();
						}
					});
				}
			};
			mTimer.schedule(mTimerTask, 3 * 1000);
		}
	}
	public void release () {
		if (mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}
		if (mTimerTask != null) {
			mTimerTask.cancel();
			mTimerTask = null;
		}
	}

	/**
	 * Rect 比较
	 * @param maxRect
	 * @param rect
	 * @return
	 */
	private boolean comparaRect (Rect maxRect, Rect rect) {
		int maxleft = maxRect.left;
		int maxTop = maxRect.top;
		int maxRight= maxRect.right;
		int maxBottom = maxRect.bottom;
		int left = rect.left;
		int top = rect.top;
		int right = rect.right;
		int bottom = rect.bottom;
		if (maxleft< left && maxTop < top && right < maxRight && bottom <maxBottom ){
			// 人脸Rect 在 标准Rect 矩形范围内
			return true;
		}
		return  false;
	}
	private DisplayMetrics getScreenWH() {
		DisplayMetrics dMetrics = new DisplayMetrics();
		dMetrics = mContext.getResources().getDisplayMetrics();
		return dMetrics;
	}


	public interface CenterFaceCallBack{
		void callBack();
	}

}
