package cn.com.artemis.biz.mgb.bean;

public class NotifyBean {
    /**
     * content : content
     * title : tile
     * type : 1
     */

    private String content;
    private String title;
    private int type;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
