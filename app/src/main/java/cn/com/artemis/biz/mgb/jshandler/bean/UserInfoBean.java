package cn.com.artemis.biz.mgb.jshandler.bean;

/**
 * Created by whh on 2018/9/8.
 */

public class UserInfoBean {

    private String token;
    private String tencetImSig;
    private UserBean userInfo;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTencetImSig() {
        return tencetImSig;
    }

    public void setTencetImSig(String tencetImSig) {
        this.tencetImSig = tencetImSig;
    }

    public UserBean getUser() {
        return userInfo;
    }

    public void setUser(UserBean user) {
        this.userInfo = user;
    }

    public static class UserBean {

        private int id;
        private String password;
        private String phone;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
