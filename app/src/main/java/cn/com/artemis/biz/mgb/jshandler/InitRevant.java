package cn.com.artemis.biz.mgb.jshandler;

import android.content.Context;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.cn.artemis.basekit.thirdpay.event.CallH5Event;
import com.cn.artemis.basekit.utils.SharedPreferencesUtil;
import com.cn.artemis.basekit.utils.SwitchLanguageUtils;
import com.cn.atemis.artemisbwv.core.X5BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import cn.com.artemis.biz.mgb.jshandler.bean.CallJsBean;


/**
 * Created by whh on 2018/8/30.
 */

public class InitRevant {

    public static final String JS_CALL_NATIVE= "jsCallNative";
    public static final String JS_CALL_SWITCH_LANGUAGE= "switchLanguage";
    public static final String NATIVE_CALL_JS= "nativeCallJs";
    public static final String NATIVE_CALL_INITNIMKEY= "initTimKey";
    public InitRevant() {
        BusHelper.registe(this);
    }

    /**
     * 初始化相关
     * @param context
     * @param bridgeWebView
     */
    public  void initLanguage(final Context context, X5BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_NATIVE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","JS Call NATIVE ....");
                CallJsBean callJsBean = new CallJsBean();
                callJsBean.setLanguage(SwitchLanguageUtils.getLanguage(context));
                CallH5Event event = new CallH5Event();
                event.methodName= NATIVE_CALL_JS;
                event.param= JsonHelper.toJSONString(callJsBean);
                BusHelper.postEvent(event);
            }
        });
    }
    /**
     * 初始化相关
     * @param context
     * @param bridgeWebView
     */
    public  void initLanguage(final Context context, BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_NATIVE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","JS Call NATIVE ....");
                CallJsBean callJsBean = new CallJsBean();
                String language = SharedPreferencesUtil.getString(context, "language", SwitchLanguageUtils.getLanguage(context));
                callJsBean.setLanguage(language);
                CallH5Event event = new CallH5Event();
                event.methodName= NATIVE_CALL_JS;
                event.param= JsonHelper.toJSONString(callJsBean);
                BusHelper.postEvent(event);
            }
        });
        bridgeWebView.registerHandler(JS_CALL_SWITCH_LANGUAGE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                //切换原生语言
                SwitchLanguageUtils.switchLanguage(context,data);
            }
        });
        bridgeWebView.registerHandler(NATIVE_CALL_INITNIMKEY, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                //初始化云信key
                setNimMeta(context,data);
            }
        });
    }
    public void release(){
        BusHelper.unRegiste(this);
    }


    private void setNimMeta(Context context, String key){
        SharedPreferencesUtil.saveNimAppkey(context,key);
    }
}
