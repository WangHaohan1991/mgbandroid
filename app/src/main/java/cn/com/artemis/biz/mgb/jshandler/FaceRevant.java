package cn.com.artemis.biz.mgb.jshandler;

import android.Manifest;
import android.content.Context;
import android.content.Intent;

import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.artemisbase.utils.other.LogUtils;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;

import cn.com.artemis.biz.mgb.MgbApplication;
import cn.com.artemis.biz.mgb.activity.FaceActivity;


public class FaceRevant {
    public static final String JS_CALL_FACE = "face";
    private Context mContext;

    public FaceRevant(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 人脸识别
     * @param bridgeWebView
     */
    public  void startFace(BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_FACE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                LogUtils.e("TGA","...调用原生人脸识别");
                MgbApplication.token = data;
                validPermission();
            }
        });
    }

    /**
     * 动态权限验证
     */
    private void validPermission() {
        Acp.getInstance(mContext).request(new AcpOptions.Builder()
                .setDeniedMessage("人脸识别需要打开相机权限")  // 以下为自定义提示语、按钮文字
                /*.setDeniedCloseBtn()
                .setDeniedSettingBtn()
                .setRationalMessage()
                .setRationalBtn()*/
                .setPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE).build(), new AcpListener() {
            @Override
            public void onGranted() {
                Intent intent = new Intent(mContext, FaceActivity.class);
                mContext.startActivity(intent);
            }
            @Override
            public void onDenied(List<String> permissions) {
                ToastSafeUtils.showLongToast(mContext,"人脸识别需要打开相机权限");
            }
        });
    }

}
