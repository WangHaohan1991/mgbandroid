package cn.com.artemis.biz.mgb.jshandler;

import android.content.Context;

import com.cn.artemis.artemisbase.dialog.MenuDialog;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.basekit.map.NavigateManager;
import com.cn.artemis.basekit.utils.ListDataManager;
import com.cn.atemis.artemisbwv.core.X5BridgeWebView;
import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import java.util.List;

import cn.com.artemis.biz.mgb.R;


/**
 * Created by whh on 2018/8/29.
 */

public class MapNavigate {

    public static final String JS_CALL_TONAVIGATE= "toNavigate";
    private Context mContext;
    public MapNavigate(Context context) {
        mContext = context;
    }

    /**
     * 地图导航
     * @param bridgeWebView
     */
    public  void toNavigate( X5BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_TONAVIGATE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                setMenu(data);
            }
        });
    }
    /**
     * 地图导航
     * @param bridgeWebView
     */
    public  void toNavigate( BridgeWebView bridgeWebView){
        bridgeWebView.registerHandler(JS_CALL_TONAVIGATE, new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                setMenu(data);
            }
        });
    }

    private void setMenu(String data) {
        final List<Double> mapList = JsonHelper.parseJSONArray(data,Double.class);
        MenuDialog menuDialog = new MenuDialog(mContext,
                ListDataManager.getListData(mContext, R.array.mapArray),
                new MenuDialog.MenuDialogOnButtonClickListener() {
            @Override
            public void onButtonClick(String name, int index) {
                goNavigate(mapList,index);
            }
        });
        menuDialog.setItemColor(R.color.colorAccent);
        menuDialog.show();
    }

    private void goNavigate(List<Double> mapList, int index) {
        switch (index){
            case 0:
                NavigateManager.goGaodeMap(mContext,mapList);
                break;
            case 1:
                NavigateManager.goBaiduMap(mContext,mapList);
                break;
            case 2:
                NavigateManager.goGoogleMap(mContext,mapList);
                break;
            case 3:
                NavigateManager.goTencentMap(mContext,mapList);
                break;
            default:
                break;
        }
    }

}
