package com.cn.artemis.basekit.thirdpay.event;

/**
 * Created by whh on 2018/2/5.
 */

public class PayPalResultEvent {
    private int resultCode;

    private String message;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
