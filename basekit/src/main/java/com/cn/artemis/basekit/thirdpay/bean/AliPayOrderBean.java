package com.cn.artemis.basekit.thirdpay.bean;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/13 20:50
 * @version V1.0
 */

/**
 * @ClassName: AliPayOrderBean
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/13 20:50
 */

public class AliPayOrderBean extends PayBaseBean {

    /**
     * data : {"orderStr":"app_id=2017091908816369&biz_content=%7B%22body%22%3A%22%E8%B4%AD%E4%B9%B0%22%2C%22subject%22%3A%22%E8%B4%AD%E4%B9%B0%22%2C%22out_trade_no%22%3A%22478954176450134016%22%2C%22timeout_express%22%3A%22lc%22%2C%22total_amount%22%3A%2218.20%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=utf-8&format=JSON&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Ftestapi.zjh888.net%3A8080%2Forder%2Fnotify%2Falipay&sign_type=RSA2&timestamp=2018-08-14+15%3A52%3A54&version=1.0&sign=UiJHu2TWIBFZRcNw7si1h3vYRiTafQbq0%2BAf3b%2Bmhq1AeuXap%2F9OaXBR29KJrsKg7%2BO4axdmhXxTliaMpSuWeGUJcsBS3K7Cb3YDBME5GkhbjLYkM81Bxj1ESJsJpBCsj0xJawlf5WOibogVpgWJaGcOIqZaB3I2%2FzqJ7BfvJelHdldBjVVcErm0WksEhVgiXXyg9cErpxxwgkWaOuki86deoIney9wODyuWDyAfhB1NMUFcvIgc%2B5NJTTbyOxj8HjTJAla6ESdnXazG%2FwNUXO8GEl48qOJKz5vSu64gWnXiSdd4lud03zJnfMQZO2kPFCRBTS1CmGx9La7ZsdX3QA%3D%3D","orderCode":"478954176450134016"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * orderStr : app_id=2017091908816369&biz_content=%7B%22body%22%3A%22%E8%B4%AD%E4%B9%B0%22%2C%22subject%22%3A%22%E8%B4%AD%E4%B9%B0%22%2C%22out_trade_no%22%3A%22478954176450134016%22%2C%22timeout_express%22%3A%22lc%22%2C%22total_amount%22%3A%2218.20%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=utf-8&format=JSON&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Ftestapi.zjh888.net%3A8080%2Forder%2Fnotify%2Falipay&sign_type=RSA2&timestamp=2018-08-14+15%3A52%3A54&version=1.0&sign=UiJHu2TWIBFZRcNw7si1h3vYRiTafQbq0%2BAf3b%2Bmhq1AeuXap%2F9OaXBR29KJrsKg7%2BO4axdmhXxTliaMpSuWeGUJcsBS3K7Cb3YDBME5GkhbjLYkM81Bxj1ESJsJpBCsj0xJawlf5WOibogVpgWJaGcOIqZaB3I2%2FzqJ7BfvJelHdldBjVVcErm0WksEhVgiXXyg9cErpxxwgkWaOuki86deoIney9wODyuWDyAfhB1NMUFcvIgc%2B5NJTTbyOxj8HjTJAla6ESdnXazG%2FwNUXO8GEl48qOJKz5vSu64gWnXiSdd4lud03zJnfMQZO2kPFCRBTS1CmGx9La7ZsdX3QA%3D%3D
         * orderCode : 478954176450134016
         */

        private String orderStr;
        private String orderCode;

        public String getOrderStr() {
            return orderStr;
        }

        public void setOrderStr(String orderStr) {
            this.orderStr = orderStr;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }
    }
}
