package com.cn.artemis.basekit.map;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;

import java.io.File;
import java.util.List;

/**
 * Created by whh on 2018/8/29.
 */

public class NavigateManager {



    public static void goGaodeMap(Context mContext, List<Double> mapList){
        if(isPackageInstalled("com.autonavi.minimap")){
            Uri uri = Uri.parse("androidamap://navi?sourceApplication=我的位置&lat="
                    + mapList.get(0)+ "&lon=" + mapList.get(1)+ "&dev=0&style=2");
            Intent naviIntent = new Intent("android.intent.action.VIEW", uri);
            naviIntent.setPackage("com.autonavi.minimap");
            mContext.startActivity(naviIntent);
        }else {
            ToastSafeUtils.showLongToast(mContext,"没有安装高德地图客户端");
        }
    }
    public static void goBaiduMap(Context mContext, List<Double> mapList){
        if(isPackageInstalled("com.baidu.BaiduMap")){
            Uri uri = Uri.parse("baidumap://map/geocoder?location="
                    + mapList.get(0) + "," + mapList.get(1));
            Intent naviIntent = new Intent("android.intent.action.VIEW", uri);
            mContext.startActivity(naviIntent);
        }else {
            ToastSafeUtils.showLongToast(mContext,"没有安装百度地图客户端");
        }
    }
    public static void goTencentMap(Context mContext, List<Double> mapList){
        if(isPackageInstalled("com.tencent.map")){
            Uri uri = Uri.parse("qqmap://map/routeplan?type=drive&from=&fromcoord=&to=目的地&tocoord="
                    + mapList.get(0) + "," + mapList.get(1)+ "&policy=0&referer=appName");
            Intent naviIntent = new Intent("android.intent.action.VIEW", uri);
            mContext.startActivity(naviIntent);
        }else {
            ToastSafeUtils.showLongToast(mContext,"没有安装腾讯地图客户端");
        }
    }
    public static void goGoogleMap(Context mContext, List<Double> mapList){
        if(isPackageInstalled("com.google.android.apps.maps")){
            Uri uri = Uri.parse("qqmap://map/routeplan?type=drive&from=&fromcoord=&to=目的地&tocoord="
                    + mapList.get(0) + "," + mapList.get(1)+ "&policy=0&referer=appName");
            Intent naviIntent = new Intent("android.intent.action.VIEW", uri);
            naviIntent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
            mContext.startActivity(naviIntent);
        }else {
            ToastSafeUtils.showLongToast(mContext,"没有安装谷歌地图客户端");
        }
    }
    private static boolean isPackageInstalled(String packageName) {
        return new File("/data/data/" + packageName).exists();
    }
}
