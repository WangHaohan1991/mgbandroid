package com.cn.artemis.basekit.thirdpay.pay;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 11:36
 * @version V1.0
 */

import android.app.Activity;

import com.cn.artemis.basekit.thirdpay.interfaces.IPayCopyStrategy;
import com.cn.artemis.basekit.thirdpay.interfaces.IPayResultCallback;
import com.cn.artemis.basekit.thirdpay.pay.base.IPayInfo;


/**
 * @ClassName: YQPay
 *Description:  1、银联使用说明：实例化银联支付策略
                                UnionCopyPay unionPay = new UnionCopyPay();
                                // 构造银联订单实体。一般都是由服务端直接返回。测试时可以用Mode.TEST,发布时用Mode.RELEASE。
                                UnionPayInfoImpli unionPayInfoImpli = new UnionPayInfoImpli();
                                unionPayInfoImpli.setTn("814144587819703061900");
                                unionPayInfoImpli.setMode(Mode.TEST);
                                // 策略场景类调起支付方法开始支付，以及接收回调。
                                YQPay.pay(unionPay, this, unionPayInfoImpli, new IPayREsultCallback<YQUnionEntity>());
                2、 微信支付使用：实例化微信支付策略
                                WechatPay wxPay = WechatPay.getInstance((Activity) context,wxAppId);
                                //构造微信订单实体。一般都是由服务端直接返回。
                                WXPayInfoImpli wxPayInfoImpli = new WXPayInfoImpli();
                                wxPayInfoImpli.setPackageValue("");
                                wxPayInfoImpli.setAppid("");
                                wxPayInfoImpli.setPartnerid("");
                                wxPayInfoImpli.setNonceStr("");
                                wxPayInfoImpli.setTimestamp("");
                                wxPayInfoImpli.setSign("");
                                wxPayInfoImpli.setPrepayId("");
                                //策略场景类调起支付方法开始支付，以及接收回调。
                                YQPay.pay(wxPay, (this, wxPayInfoImpli,new IPayREsultCallback<YQWechatEntity>());
                3、支付宝使用说明：实例化支付宝支付策略
                                AliCopyPay aliPay = new AliCopyPay();
                                //构造支付宝订单实体。一般都是由服务端直接返回。
                                AlipayInfoImpli alipayInfoImpli = new AlipayInfoImpli();
                                alipayInfoImpli.setOrderInfo("");
                                //策略场景类调起支付方法开始支付，以及接收回调。
                                YQPay.pay(aliPay, (this, alipayInfoImpli,new IPayREsultCallback<YQAliEntity>());
                                }
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 11:36
 */

public class YQPay {
    public static <T extends IPayInfo> void pay(IPayCopyStrategy<T> payWay, Activity mActivity, T payinfo, IPayResultCallback callback){
        payWay.pay(mActivity, payinfo, callback);
    }
}
