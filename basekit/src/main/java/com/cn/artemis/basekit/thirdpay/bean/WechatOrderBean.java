package com.cn.artemis.basekit.thirdpay.bean;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/13 20:49
 * @version V1.0
 */

import com.google.gson.annotations.SerializedName;

/**
 * @ClassName: WechatOrderBean
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/13 20:49
 */

public class WechatOrderBean extends PayBaseBean {
    /**
     * package : Sign=WXPay
     * appid : wx5126f6a9301e1c9c
     * sign : 0775F9BAC933AAEE9DC242F695F6266C
     * orderCode : 324289026125201408
     * partnerid : 1429161402
     * prepayid : wx2017061320485741931d38c60177467677
     * noncestr : Hzyq8UQCSS9Xwd3S0MlL
     * timestamp : 1497358129
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("package")
        private String packageX;
        private String appid;
        private String sign;
        private String orderCode;
        private String partnerid;
        private String prepayid;
        private String noncestr;
        private String timestamp;

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }
}
