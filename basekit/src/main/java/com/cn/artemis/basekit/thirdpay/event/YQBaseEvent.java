package com.cn.artemis.basekit.thirdpay.event;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2016
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/4/17 10:15
 * @version V1.0
 */

import android.text.TextUtils;

import com.cn.artemis.artemisbase.events.BaseEvent;
import com.cn.artemis.artemisbase.network.json.JsonHelper;
import com.cn.artemis.artemisbase.utils.other.LogUtils;


/**
 * @author android移动客户端-王浩韩
 * @ClassName: BaseEvent
 * Description: TODO
 * @date 2017/4/17 10:15
 */

public class YQBaseEvent extends BaseEvent {
    //请求成功的返回code
    public static final int SUCCESS_CODE = 200;

    public <T> T getModel(Class<T> tClass) {
        if (isSuccess()) {
            if (!TextUtils.isEmpty(getData())) {
                LogUtils.i(tClass.getSimpleName() + "JSON请求结果--->" + getData());
                T t = JsonHelper.parseObject(getData(), tClass);
                return t;
            }
        } else {
            LogUtils.e(tClass.getSimpleName() + "Error请求结果--->" + getError().getMessage());
        }
        return null;
    }
}
