package com.cn.artemis.basekit.thirdpay.interfaces;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 11:28
 * @version V1.0
 */


import com.cn.artemis.basekit.thirdpay.model.YQEntity;

/**
 * @ClassName: IPayResultCallback
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 11:28
 */

public interface IPayResultCallback<T extends YQEntity> {
    void success(T t);
    void failed(String message);
    void cancel();
}
