package com.cn.artemis.basekit.thirdpay.pay;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 16:39
 * @version V1.0
 */

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.artemisbase.utils.common.ToastSafeUtils;
import com.cn.artemis.basekit.thirdpay.bean.AliPayOrderBean;
import com.cn.artemis.basekit.thirdpay.bean.PayPalOrderBean;
import com.cn.artemis.basekit.thirdpay.bean.WechatOrderBean;
import com.cn.artemis.basekit.thirdpay.interfaces.IPayResultCallback;
import com.cn.artemis.basekit.thirdpay.interfaces.OnPayResultListener;
import com.cn.artemis.basekit.thirdpay.model.YQAliEntity;
import com.cn.artemis.basekit.thirdpay.model.YQPayPalEntity;
import com.cn.artemis.basekit.thirdpay.model.YQUnionEntity;
import com.cn.artemis.basekit.thirdpay.model.YQWechatEntity;
import com.cn.artemis.basekit.thirdpay.pay.alipay.AlipayInfoImpli;
import com.cn.artemis.basekit.thirdpay.pay.wechatpay.wxpay.WXPayInfoImpli;


/**
 * @ClassName: ThirdPayManager
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 16:39
 */

public class ThirdPayManager {
    //支付宝
    public static final int ALIPAY = 1;
    //微信
    public static final int WXPAY = 2;
    //PayPal
    public static final int PAYPAL = 3;

    private Context mContext;

    private OnPayResultListener mls;

    public ThirdPayManager(Context context) {
        mContext = context;
        BusHelper.registe(this);
    }
    public  void release(){
        BusHelper.unRegiste(this);
    }

    public void setOnPayResultListener(OnPayResultListener mls) {
        this.mls = mls;
    }


    /**
     * 微信支付
     * @param dataBean
     */
    public void payByWechatDirectly(WechatOrderBean.DataBean dataBean) {
        if (dataBean != null){
            String wxAppId = dataBean.getAppid();
            WechatPay wxPay = WechatPay.getInstance((Activity) mContext,wxAppId);
            //构造微信订单实体。一般都是由服务端直接返回。
            WXPayInfoImpli wxPayInfoImpli = new WXPayInfoImpli();
            wxPayInfoImpli.setPackageValue(dataBean.getPackageX());
            wxPayInfoImpli.setAppid(dataBean.getAppid());
            wxPayInfoImpli.setPartnerid(dataBean.getPartnerid());
            wxPayInfoImpli.setNonceStr(dataBean.getNoncestr());
            wxPayInfoImpli.setTimestamp(dataBean.getTimestamp());
            wxPayInfoImpli.setSign(dataBean.getSign());
            wxPayInfoImpli.setPrepayId(dataBean.getPrepayid());
            //策略场景类调起支付方法开始支付，以及接收回调。
            YQPay.pay(wxPay, (Activity) mContext, wxPayInfoImpli,mWXResultCallback);
        }
    }

    /**
     * 通过PayPal直接支付
     */
    public void payByPayPalDirectly(PayPalOrderBean payPalOrderBean){
        if (payPalOrderBean != null){
            PayPalPay payPalPay = new PayPalPay(mContext);
            payPalPay.pay(payPalOrderBean,mPayPalResultCallback);
        }else {
            ToastSafeUtils.showLongToast(mContext,"支付失败");
        }

    }

    /**
     * 通过支付宝bean直接支付
     * @param dataBean
     */
    public void payByAliDirectly(AliPayOrderBean.DataBean dataBean) {
        if (dataBean != null){
            AliCopyPay aliPay = new AliCopyPay();
            //构造支付宝订单实体。一般都是由服务端直接返回。
            AlipayInfoImpli alipayInfoImpli = new AlipayInfoImpli();
            alipayInfoImpli.setOrderInfo(dataBean.getOrderStr());
            //策略场景类调起支付方法开始支付，以及接收回调。
            YQPay.pay(aliPay, (Activity) mContext, alipayInfoImpli,mAliResultCallback );
        }
    }

    public void payByAliDirectlyStr(String string){
        AliCopyPay aliPay = new AliCopyPay();
        //构造支付宝订单实体。一般都是由服务端直接返回。
        AlipayInfoImpli alipayInfoImpli = new AlipayInfoImpli();
        alipayInfoImpli.setOrderInfo(string);
        //策略场景类调起支付方法开始支付，以及接收回调。
        YQPay.pay(aliPay, (Activity) mContext, alipayInfoImpli,mAliResultCallback );
    }

    /**
     * PayPal支付结果
     */
    private IPayResultCallback<YQPayPalEntity> mPayPalResultCallback = new IPayResultCallback<YQPayPalEntity>() {
        @Override
        public void success(YQPayPalEntity yqPayPalEntity) {
            //支付成功
            if (mls!= null){
                mls.paySuccess();
            }
        }

        @Override
        public void failed(String message) {
            if (!TextUtils.isEmpty(message)){
                //支付失败
                ToastSafeUtils.showShortToast(mContext,message);
            }
            if (mls!= null){
                mls.payFailed();
            }
        }

        @Override
        public void cancel() {
            //支付取消
            ToastSafeUtils.showShortToast(mContext,"支付取消");
            if (mls!= null){
                mls.payCancel();
            }
        }
    };

    /**
     * aliPay支付结果
     */
    private IPayResultCallback<YQAliEntity> mAliResultCallback = new IPayResultCallback<YQAliEntity>() {
        @Override
        public void success(YQAliEntity yqAliEntity) {
            //支付成功
            if (mls!= null){
                mls.paySuccess();
            }
        }
        @Override
        public void failed(String message) {
            if (!TextUtils.isEmpty(message)){
                //支付失败
                ToastSafeUtils.showShortToast(mContext,message);
            }
            if (mls!= null){
                mls.payFailed();
            }
        }
        @Override
        public void cancel() {
            //支付取消
            ToastSafeUtils.showShortToast(mContext,"支付取消");
            if (mls!= null){
                mls.payCancel();
            }
        }
    };

    /**
     * 微信支付结果
     */
    private IPayResultCallback<YQWechatEntity> mWXResultCallback = new IPayResultCallback<YQWechatEntity>() {
        @Override
        public void success(YQWechatEntity yqWechatEntity) {
            if (mls != null) {
                mls.paySuccess();
            }
        }

        @Override
        public void failed(String message) {
            if (!TextUtils.isEmpty(message)) {
                //支付失败
                ToastSafeUtils.showShortToast(mContext, message);
            }
            if (mls != null) {
                mls.payFailed();
            }
        }

        @Override
        public void cancel() {
            //支付取消
            ToastSafeUtils.showShortToast(mContext,"支付取消");
            if (mls!= null){
                mls.payCancel();
            }
        }
    };

    /**
     * 银联支付结果
     */
        private IPayResultCallback<YQUnionEntity> mUnionResultCallback = new IPayResultCallback<YQUnionEntity>() {
        @Override
        public void success(YQUnionEntity yqUnionEntity) {
            if (mls!= null){
                mls.paySuccess();
            }
        }

        @Override
        public void failed(String message) {
            if (!TextUtils.isEmpty(message)){
                //支付失败
                ToastSafeUtils.showShortToast(mContext,message);
            }
            if (mls!= null){
                mls.payFailed();
            }
        }

        @Override
        public void cancel() {
            //支付取消
            ToastSafeUtils.showShortToast(mContext,"支付取消");
            if (mls!= null){
                mls.payCancel();
            }
        }
    };
}
