package com.cn.artemis.basekit.thirdpay.model;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 12:02
 * @version V1.0
 */


import com.tencent.mm.opensdk.modelbase.BaseResp;

/**
 * @ClassName: YQWechatEntity
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 12:02
 */

public class YQWechatEntity extends YQEntity {
    private BaseResp mBaseResp;

    public BaseResp getBaseResp() {
        return mBaseResp;
    }

    public void setBaseResp(BaseResp baseResp) {
        mBaseResp = baseResp;
    }
}
