package com.cn.artemis.basekit.thirdpay.model;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 15:57
 * @version V1.0
 */

/**
 * @ClassName: YQUnionEntity
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 15:57
 */

public class YQUnionEntity extends YQEntity {
    String sign ;
    String dataOrg ;
    // 验签证书同后台验签证书
    // 此处的verify，商户需送去商户后台做验签
    boolean ret;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getDataOrg() {
        return dataOrg;
    }

    public void setDataOrg(String dataOrg) {
        this.dataOrg = dataOrg;
    }

    public boolean isRet() {
        return ret;
    }

    public void setRet(boolean ret) {
        this.ret = ret;
    }
}
