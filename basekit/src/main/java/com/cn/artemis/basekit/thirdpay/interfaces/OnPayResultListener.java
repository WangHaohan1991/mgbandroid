package com.cn.artemis.basekit.thirdpay.interfaces;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 16:40
 * @version V1.0
 */

/**
 * @ClassName: OnPayResultListener
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 16:40
 */

public interface OnPayResultListener {
    void paySuccess();
    void payFailed();
    void payCancel();
}
