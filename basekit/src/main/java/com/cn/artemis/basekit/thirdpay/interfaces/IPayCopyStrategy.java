package com.cn.artemis.basekit.thirdpay.interfaces;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 11:31
 * @version V1.0
 */

import android.app.Activity;

import com.cn.artemis.basekit.thirdpay.pay.base.IPayInfo;


/**
 * @ClassName: IPayCopyStrategy
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 11:31
 */

public interface IPayCopyStrategy <T extends IPayInfo>{
    void pay(Activity activity, T payInfo, IPayResultCallback payCallback);
}
