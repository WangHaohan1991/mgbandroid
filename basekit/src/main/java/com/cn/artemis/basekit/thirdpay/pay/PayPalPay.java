package com.cn.artemis.basekit.thirdpay.pay;

import android.app.Activity;
import android.content.Context;

import com.cn.artemis.artemisbase.events.bus.BusHelper;
import com.cn.artemis.basekit.thirdpay.activity.PayPalActivity;
import com.cn.artemis.basekit.thirdpay.bean.PayPalOrderBean;
import com.cn.artemis.basekit.thirdpay.event.PayPalResultEvent;
import com.cn.artemis.basekit.thirdpay.interfaces.IPayResultCallback;
import com.cn.artemis.basekit.thirdpay.model.YQPayPalEntity;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.paypal.android.sdk.payments.PaymentActivity;


/**
 * Created by whh on 2018/2/5.
 */

public class PayPalPay {

    private Context mContext;

    private IPayResultCallback<YQPayPalEntity> mYQPayPalEntityIPayResultCallback;

    private PayPalOrderBean mOrderBean;


    public PayPalPay(Context context) {
        mContext = context;
        BusHelper.registe(this);
    }

    public void pay(PayPalOrderBean payPalOrderBean,IPayResultCallback<YQPayPalEntity> YQPayPalEntityIPayResultCallback) {
        this.mOrderBean = payPalOrderBean;
        mYQPayPalEntityIPayResultCallback = YQPayPalEntityIPayResultCallback;
        startPay();
    }

    private void startPay() {
        PayPalActivity.startPayActivity(mContext,mOrderBean);
    }

    public void relese(){
        BusHelper.unRegiste(this);
    }
    @Subscribe
    public void payPalResult(PayPalResultEvent palResultEvent){
        if (palResultEvent != null){
            switch (palResultEvent.getResultCode()){
                case Activity.RESULT_OK:
                    if (mYQPayPalEntityIPayResultCallback != null)
                        mYQPayPalEntityIPayResultCallback.success(null);
                    break;
                case Activity.RESULT_CANCELED:
                    if (mYQPayPalEntityIPayResultCallback != null)
                        mYQPayPalEntityIPayResultCallback.cancel();
                    break;
                case PaymentActivity.RESULT_EXTRAS_INVALID:
                    if (mYQPayPalEntityIPayResultCallback != null)
                        mYQPayPalEntityIPayResultCallback.failed(palResultEvent.getMessage());
                    break;
                default:
                    break;
            }
        }
    }
}
