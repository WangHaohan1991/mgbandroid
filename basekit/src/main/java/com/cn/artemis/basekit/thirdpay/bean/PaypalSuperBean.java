package com.cn.artemis.basekit.thirdpay.bean;

/**
 * Created by whh on 2018/8/14.
 */

public class PaypalSuperBean extends PayBaseBean {
    private PayPalOrderBean data;

    public PayPalOrderBean getData() {
        return data;
    }

    public void setData(PayPalOrderBean data) {
        this.data = data;
    }
}
