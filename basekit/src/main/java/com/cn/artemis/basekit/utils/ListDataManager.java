package com.cn.artemis.basekit.utils;
/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/4/24 14:43
 * @version V1.0
 */

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ListDataManager
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/4/24 14:43
 */

public class ListDataManager {


    /**
     * 获取列表数据
     * @param context
     * @return
     */
    public static List<String> getListData(Context context,int res) {
        String[]  strings = context.getResources().getStringArray(res);
        List<String> list = new ArrayList();
        for (int i = 0; i <strings .length ; i++) {
            list.add(strings[i]);
        }
        return list;
    }


}
