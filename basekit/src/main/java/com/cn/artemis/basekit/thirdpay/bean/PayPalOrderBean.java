package com.cn.artemis.basekit.thirdpay.bean;

import java.io.Serializable;

/**
 * Created by whh on 2018/2/5.
 */

public class PayPalOrderBean implements Serializable{
    /**
     * clientId : AfKgvWebfKBcCjDsJX78mqOnJc4-cISa3PQ6Ze_tz3wUyRyLWdouPAPNULpZpqhI26AMVXN4g6i0Po5Y
     * money : 0.01
     * orderCode : 410032553412526080
     * currency : USD
     * payChanel : 2
     */

    private String clientId;
    private double money;
    private String orderCode;
    private String currency;
    private int payChanel;
    private String env;


    private String skuName;

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPayChanel() {
        return payChanel;
    }

    public void setPayChanel(int payChanel) {
        this.payChanel = payChanel;
    }
}
