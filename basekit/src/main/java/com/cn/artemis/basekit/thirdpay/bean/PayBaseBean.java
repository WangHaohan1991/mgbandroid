package com.cn.artemis.basekit.thirdpay.bean;

/**
 * Created by whh on 2018/8/13.
 */

public class PayBaseBean {

    /**
     * code : 200
     * data : {"payImgUrl":"http://qr.liantu.com/api.php?text=https://qr.alipay.com/bax0270602var2mnciev20dc","payUrl":"https://qr.alipay.com/bax0270602var2mnciev20dc"}
     * payChanel : 3
     */

    private int code;
    private int payChanel;
    private  String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPayChanel() {
        return payChanel;
    }

    public void setPayChanel(int payChanel) {
        this.payChanel = payChanel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
