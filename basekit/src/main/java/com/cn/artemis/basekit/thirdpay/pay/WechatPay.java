package com.cn.artemis.basekit.thirdpay.pay;

import android.app.Activity;
import android.text.TextUtils;

import com.cn.artemis.basekit.thirdpay.interfaces.IPayCopyStrategy;
import com.cn.artemis.basekit.thirdpay.interfaces.IPayResultCallback;
import com.cn.artemis.basekit.thirdpay.model.YQWechatEntity;
import com.cn.artemis.basekit.thirdpay.pay.wechatpay.wxpay.WXPay;
import com.cn.artemis.basekit.thirdpay.pay.wechatpay.wxpay.WXPayInfoImpli;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;


/**
 * @Description: TODO
 * Copyright: Copyright (c) 2017
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2017/6/14 11:25
 * @version V1.0
 */

/**
 * @ClassName: WechatPay
 *Description: TODO
 *@author android移动客户端-王浩韩
 * @date 2017/6/14 11:25
 */

public class WechatPay implements IPayCopyStrategy<WXPayInfoImpli> {
    private static WechatPay mWXPay;
    private WXPayInfoImpli payInfoImpli;
    private static IPayResultCallback sPayCallback;
    private IWXAPI mWXApi;

    private WechatPay(Activity mActivity, String wxAppId) {
        mWXApi = WXAPIFactory.createWXAPI(mActivity.getApplicationContext(), null);
        mWXApi.registerApp(wxAppId);
    }

    public static WechatPay getInstance(Activity mActivity, String wxAppId){
        if(mWXPay == null){
            synchronized (WXPay.class){
                if(mWXPay == null) {
                    mWXPay = new WechatPay(mActivity,wxAppId);
                }
            }
        }
        return mWXPay;
    }

    public IWXAPI getWXApi() {
        return mWXApi;
    }

    @Override
    public void pay(Activity activity, WXPayInfoImpli payInfo, IPayResultCallback payCallback) {
        this.payInfoImpli = payInfo;
        sPayCallback = payCallback;
        if(!check()) {
            if(payCallback != null) {
                payCallback.failed("目前您的微信版本过低或未安装微信，需要安装微信才能使用");
            }
            return;
        }
        if(payInfoImpli == null || TextUtils.isEmpty(payInfoImpli.getAppid()) || TextUtils.isEmpty(payInfoImpli.getPartnerid())
                || TextUtils.isEmpty(payInfoImpli.getPrepayId()) || TextUtils.isEmpty(payInfoImpli.getPackageValue()) ||
                TextUtils.isEmpty(payInfoImpli.getNonceStr()) || TextUtils.isEmpty(payInfoImpli.getTimestamp()) ||
                TextUtils.isEmpty(payInfoImpli.getSign())) {
            if(payCallback != null) {
                payCallback.failed("订单信息不完善");
            }
            return;
        }

        PayReq req = new PayReq();
        req.appId = payInfoImpli.getAppid();
        req.partnerId = payInfoImpli.getPartnerid();
        req.prepayId = payInfoImpli.getPrepayId();
        req.packageValue = payInfoImpli.getPackageValue();
        req.nonceStr = payInfoImpli.getNonceStr();
        req.timeStamp = payInfoImpli.getTimestamp();
        req.sign = payInfoImpli.getSign();

        mWXApi.sendReq(req);
    }

    //支付回调响应
    public void onResp(BaseResp baseResp) {
        if(sPayCallback == null) {
            return;
        }

        if(baseResp.errCode == 0) {   //成功
            YQWechatEntity yqWechatEntity = new YQWechatEntity();
            yqWechatEntity.setBaseResp(baseResp);
            sPayCallback.success(yqWechatEntity);
        } else if(baseResp.errCode== -1) {   //错误
            sPayCallback.failed(baseResp.errStr);
        } else if(baseResp.errCode == -2) {   //取消
            sPayCallback.cancel();
        }

        sPayCallback = null;
    }

    //检测是否支持微信支付
    private boolean check() {
        return mWXApi.isWXAppInstalled() && mWXApi.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
    }
}
