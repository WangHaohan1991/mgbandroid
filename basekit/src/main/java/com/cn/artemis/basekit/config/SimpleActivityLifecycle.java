package com.cn.artemis.basekit.config;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.cn.artemis.artemisbase.utils.other.LogUtils;

/**
 * Created by whh on 2018/1/12.
 */

public class SimpleActivityLifecycle implements Application.ActivityLifecycleCallbacks{

    private static final String TAG = SimpleActivityLifecycle.class.getSimpleName();

    private boolean isForeground = false;//应用是否处于前端


    public void setActivityLifecycleCAllBacks(Context context) {
        ((Application)context).registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        LogUtils.i(TAG,"----------------------------onActivityCreated");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        LogUtils.i(TAG,"----------------------------onActivityStarted");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        isForeground = true;
        LogUtils.i(TAG,"----------------------------onActivityResumed");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isForeground = false;
        LogUtils.i(TAG,"----------------------------onActivityPaused");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        LogUtils.i(TAG,"----------------------------onActivityStopped");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        LogUtils.i(TAG,"----------------------------onActivityCreated");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        LogUtils.i(TAG,"----------------------------onActivityCreated");
    }

    public boolean isForeground() {
        return isForeground;
    }
}
