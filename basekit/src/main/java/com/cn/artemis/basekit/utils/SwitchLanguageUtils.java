package com.cn.artemis.basekit.utils;/**
 * @Description: TODO
 * Copyright: Copyright (c) 2016
 * Company: 成都壹柒互动科技有限公司
 * @author android客户端-王浩韩
 * @date 2016/7/12 11:55
 * @version V1.0
 */

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

/**
 * @ClassName: SwitchLanguageUtils
 *Description: TODO
 *@author android移动客户端-王浩韩
 */
public class SwitchLanguageUtils {

    /**
     * 获取当前语言
     * */
    public static  String getCurrentLanguage(Context mContext) {
        Locale locale=mContext.getResources().getConfiguration().locale;
        Log.d("whh","locale="+locale);
        String language=locale.getLanguage();
        return language;
    }
    /**
     * 获取当前语言
     * */
    public static  String getCurrentLocale(Context mContext) {
        Locale locale=mContext.getResources().getConfiguration().locale;
        String localeStr=locale.toString();
        return localeStr;
    }
    /**
     * 获取当前语言
     * */
    public static  String getSimplifiedLanguage(Context mContext) {
        Locale locale=mContext.getResources().getConfiguration().locale;
        String language=locale.getLanguage();
        return language;
    }


    public static String  getLanguage(Context mContext){
        String  localLanguage = getCurrentLocale(mContext);
        if (localLanguage.contains("CN")||localLanguage.contains("zh_CN")||localLanguage.contains("TW")||localLanguage.contains("zh_TW")) {
            return "zh_CN";
        }else if(localLanguage.contains("en")) {
            return "en_US";
        }else {
            return "en_US";
        }
    }

    /**
     * 切换语言
     * */
    public static  void switchLanguage(Context mContext,String language){
        //设置应用语言类型
        Resources resources=mContext.getResources();
        Configuration config=resources.getConfiguration();
        DisplayMetrics dm=resources.getDisplayMetrics();
        switch (language){
            case "en_US":
            case "en_UK":
            case "en":
                config.locale=Locale.ENGLISH;
                break;
            case "zh_CN":
                config.locale=Locale.SIMPLIFIED_CHINESE;
                break;
            case "zh_TW":
                config.locale=Locale.TRADITIONAL_CHINESE;
                break;
            default:
                config.locale=Locale.ENGLISH;
                break;
        }
        resources.updateConfiguration(config,dm);
        //保存设置语言的类型
        SharedPreferencesUtil.putString(mContext, "language",language);
    }
}
